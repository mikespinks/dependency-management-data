package domain

// SBOMDependency defines the domain model for all representations of Software Bill of Materials (SBOM) derived package dependencies, which is consistent across all supported types of SBOMs
type SBOMDependency struct {
	Platform     string
	Organisation string
	Repo         string

	PackageName    string
	Version        *string
	CurrentVersion *string

	PackageType string
}
