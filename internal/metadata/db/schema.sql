CREATE TABLE IF NOT EXISTS metadata (
  name TEXT NOT NULL,
  value TEXT NOT NULL,

  UNIQUE (name) ON CONFLICT REPLACE
);
