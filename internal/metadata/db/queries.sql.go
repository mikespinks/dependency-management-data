// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.21.0
// source: queries.sql

package db

import (
	"context"
)

const retrieveDMDVersion = `-- name: RetrieveDMDVersion :one
select
  name,
  value
from
  metadata
where
  name = 'dmd_version'
`

func (q *Queries) RetrieveDMDVersion(ctx context.Context) (Metadatum, error) {
	row := q.db.QueryRowContext(ctx, retrieveDMDVersion)
	var i Metadatum
	err := row.Scan(&i.Name, &i.Value)
	return i, err
}

const setDMDVersion = `-- name: SetDMDVersion :exec
insert into metadata (
  name,
  value
) VALUES (
  'dmd_version',
  ?
)
`

func (q *Queries) SetDMDVersion(ctx context.Context, value string) error {
	_, err := q.db.ExecContext(ctx, setDMDVersion, value)
	return err
}
