-- name: RetrieveDMDVersion :one
select
  name,
  value
from
  metadata
where
  name = 'dmd_version';

-- name: SetDMDVersion :exec
insert into metadata (
  name,
  value
) VALUES (
  'dmd_version',
  ?
);
