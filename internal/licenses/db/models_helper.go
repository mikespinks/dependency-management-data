package db

import "strings"

func NewRetrieveLicenseOverviewParams(platform, org, repo, owner string) RetrieveLicenseOverviewParams {
	params := RetrieveLicenseOverviewParams{
		Platform: "%",
		Org:      "%",
		Repo:     "%",
		Owner:    "%",
	}

	if platform != "" {
		params.Platform = strings.ReplaceAll(platform, "*", "%")
	}

	if org != "" {
		params.Org = strings.ReplaceAll(org, "*", "%")
	}

	if repo != "" {
		params.Repo = strings.ReplaceAll(repo, "*", "%")
	}

	if owner != "" {
		params.Owner = strings.ReplaceAll(owner, "*", "%")
	}

	return params
}
