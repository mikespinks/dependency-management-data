package licenses

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"
	"strings"

	"dmd.tanna.dev/internal/licenses/db"
	"github.com/jedib0t/go-pretty/v6/table"
)

func ReportLicensingOverview(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, platform, org, repo, owner string) (table.Writer, error) {
	queries := db.New(sqlDB)

	tw := table.NewWriter()

	tw.AppendHeader(table.Row{
		"Platform",
		"Organisation",
		"Repo",
		"Owner",
		"Licenses",
	})

	rows, err := queries.RetrieveLicenseOverview(ctx, db.NewRetrieveLicenseOverviewParams(platform, org, repo, owner))
	if err != nil {
		return nil, err
	}

	for _, row := range rows {
		tw.AppendRow(table.Row{
			row.Platform,
			row.Organisation,
			row.Repo,
			row.Owner.String,
			csvToList(row.Licenses),
		})
	}

	return tw, nil
}

func csvToList(s string) string {
	parts := strings.Split(s, ",")
	return fmt.Sprintf("- %s", strings.Join(parts, "\n- "))
}
