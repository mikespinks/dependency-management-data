package view

import (
	"io"

	"github.com/jedib0t/go-pretty/v6/progress"
)

func NewProgressWriter(writer io.Writer, noProgress bool) progress.Writer {
	pw := progress.NewWriter()
	if noProgress {
		pw.SetOutputWriter(io.Discard)
	} else {
		pw.SetOutputWriter(writer)
	}
	pw.SetMessageWidth(80)

	return pw
}
