// Repositories contains a generic interface for interacting with repositories of data.
// Repositories may sit within their own packages, and may also be a "datasource".
package repositories
