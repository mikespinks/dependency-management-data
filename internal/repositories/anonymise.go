package repositories

import (
	"context"
	"database/sql"
)

func AnonymiseData(ctx context.Context, db *sql.DB, orgs []string) (err error) {
	for _, r := range repositories {
		err = r.AnonymiseData(ctx, db, orgs)
		if err != nil {
			return
		}
	}

	return
}
