package depsdev

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"slices"
	"strings"

	renovatedb "dmd.tanna.dev/internal/datasources/renovate/db"
	sbomdb "dmd.tanna.dev/internal/datasources/sbom/db"
)

const baseURL = "https://api.deps.dev/v3alpha"

type PackageVersionQuery struct {
	System  string
	Name    string
	Version string
}

func newRenovatePackageVersionQuery(dep renovatedb.RetrieveDistinctPackagesRow) (q PackageVersionQuery, ok bool) {
	q.Name = dep.PackageName

	q.Version = dep.Version
	if dep.CurrentVersion.Valid {
		q.Version = dep.CurrentVersion.String
	}

	if q.Version == "" {
		return
	}

	// use Datasource as a more predictable source of package ecosystems, as we could have i.e.
	//   Datasource: "maven"
	//   PackageManager: "maven", "gradle", "sbt"
	switch dep.Datasource {
	case "go":
		q.System = "GO"
		ok = true
	case "npm":
		q.System = "NPM"
		ok = true
	case "crate":
		q.System = "CARGO"
		ok = true
	case "maven":
		q.System = "MAVEN"
		ok = true
	case "pypi":
		q.System = "PYPI"
		ok = true
	}

	return
}

func newSBOMPackageVersionQuery(dep sbomdb.RetrieveDistinctPackagesRow) (q PackageVersionQuery, ok bool) {
	q.Name = dep.PackageName

	if dep.CurrentVersion.Valid {
		q.Version = dep.CurrentVersion.String
	}
	if dep.Version.Valid {
		q.Version = dep.Version.String
	}

	if q.Version == "" {
		return
	}

	switch dep.PackageType {
	case "golang":
		q.System = "GO"
		ok = true
	case "npm":
		q.System = "NPM"
		ok = true
	// TODO: Cargo not yet implemented
	case "maven":
		q.System = "MAVEN"
		ok = true
	case "pypi":
		q.System = "PYPI"
		ok = true
	}

	return
}

type DependenciesQuery struct {
	System  string
	Name    string
	Version string
}

func newRenovateDependenciesQuery(dep renovatedb.Renovate) (q DependenciesQuery, ok bool) {
	q.Name = dep.PackageName

	q.Version = dep.Version
	if dep.CurrentVersion.Valid {
		q.Version = dep.CurrentVersion.String
	}

	if q.Version == "" {
		return
	}

	var depTypes []string
	_ = json.Unmarshal([]byte(dep.DepTypes), &depTypes)

	// use Datasource as a more predictable source of package ecosystems, as we could have i.e.
	//   Datasource: "maven"
	//   PackageManager: "maven", "gradle", "sbt"
	switch dep.Datasource {
	case "maven":
		// skip Gradle plugins
		if dep.PackageManager == "gradle" && slices.Contains(depTypes, "plugin") {
			return q, false
		}

		// skip lookup of uninterpolated properties
		if strings.HasPrefix(q.Name, "${") || strings.HasPrefix(q.Version, "${") {
			return q, false
		}

		// skip lookup of SNAPSHOT versions
		if strings.HasSuffix(q.Version, "-SNAPSHOT") {
			return q, false
		}

		q.System = "MAVEN"
		ok = true
	}

	return
}

func newSBOMDependenciesQuery(dep sbomdb.Sbom) (q DependenciesQuery, ok bool) {
	q.Name = dep.PackageName

	if dep.CurrentVersion.Valid {
		q.Version = dep.CurrentVersion.String
	}
	if dep.Version.Valid {
		q.Version = dep.Version.String
	}

	if q.Version == "" {
		return
	}

	switch dep.PackageType {
	case "maven":
		// skip lookup of SNAPSHOT versions
		if strings.HasSuffix(q.Version, "-SNAPSHOT") {
			return q, false
		}

		q.System = "MAVEN"
		ok = true
	}

	return
}

type Client struct {
	httpClient *http.Client
}

func NewClient(httpClient *http.Client) Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	return Client{
		httpClient: httpClient,
	}
}

type GetVersionResponse struct {
	VersionKey struct {
		System  string `json:"system"`
		Name    string `json:"name"`
		Version string `json:"version"`
	} `json:"versionKey"`
	Licenses     []string `json:"licenses"`
	AdvisoryKeys []struct {
		ID string `json:"id"`
	} `json:"advisoryKeys"`
}

func (c *Client) GetVersion(q PackageVersionQuery) (resp GetVersionResponse, err error, notFound bool) {
	u := fmt.Sprintf("%s/systems/%s/packages/%s/versions/%s", baseURL, q.System, url.PathEscape(q.Name), url.PathEscape(q.Version))

	httpResp, err := c.httpClient.Get(u)
	if err != nil {
		return
	}
	defer httpResp.Body.Close()

	if httpResp.StatusCode != 200 {
		notFound = httpResp.StatusCode == http.StatusNotFound

		return resp, fmt.Errorf("received an HTTP %d response query GetVersion %v", httpResp.StatusCode, q), notFound
	}

	body, err := io.ReadAll(httpResp.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, &resp)
	if err != nil {
		return
	}

	return
}

type GetDependenciesResponse struct {
	Nodes []struct {
		VersionKey struct {
			System  string `json:"system"`
			Name    string `json:"name"`
			Version string `json:"version"`
		} `json:"versionKey"`
		Relation DependenciesRelation `json:"relation"`
	} `json:"nodes"`
}

type DependenciesRelation = string

const (
	DependenciesRelationSelf     = "SELF"
	DependenciesRelationDirect   = "DIRECT"
	DependenciesRelationIndirect = "INDIRECT"
)

func (c *Client) GetDependencies(q DependenciesQuery) (resp GetDependenciesResponse, err error, notFound bool) {
	u := fmt.Sprintf("%s/systems/%s/packages/%s/versions/%s:dependencies", baseURL, q.System, url.PathEscape(q.Name), url.PathEscape(q.Version))

	httpResp, err := c.httpClient.Get(u)
	if err != nil {
		return
	}
	defer httpResp.Body.Close()

	if httpResp.StatusCode != 200 {
		notFound = httpResp.StatusCode == http.StatusNotFound

		return resp, fmt.Errorf("received an HTTP %d response query GetVersion %v", httpResp.StatusCode, q), notFound
	}

	body, err := io.ReadAll(httpResp.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, &resp)
	if err != nil {
		return
	}

	return
}
