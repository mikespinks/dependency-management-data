-- name: InsertCve :exec
INSERT INTO depsdev_cves (
  package_name,
  version,

  cve_id,

  updated_at
  ) VALUES (
  ?,
  ?,

  ?,

  ?
);

-- name: InsertLicense :exec
INSERT INTO depsdev_licenses (
  package_name,
  version,

  license,

  updated_at
  ) VALUES (
  ?,
  ?,

  ?,

  ?
);

-- name: AnonymiseCvesPackageNameByOrg :exec
update depsdev_cves
set
  package_name  = 'ANON-' || hex(sha3(package_name))
where
    package_name like '%' || ? || '%'
;

-- name: AnonymiseLicensesPackageNameByOrg :exec
update depsdev_licenses
set
  package_name  = 'ANON-' || hex(sha3(package_name))
where
    package_name like '%' || ? || '%'
;
