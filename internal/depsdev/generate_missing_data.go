package depsdev

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"sync"

	renovatedb "dmd.tanna.dev/internal/datasources/renovate/db"
	sbomdb "dmd.tanna.dev/internal/datasources/sbom/db"
	"github.com/jedib0t/go-pretty/v6/progress"
	"golang.org/x/sync/errgroup"
)

func GenerateMissingData(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer, httpClient *http.Client) error {
	client := NewClient(httpClient)

	err := generateMissingRenovateData(ctx, logger, sqlDB, pw, client)
	if err != nil {
		return err
	}

	err = generateMissingSBOMData(ctx, logger, sqlDB, pw, client)
	if err != nil {
		return err
	}

	return nil
}

func generateMissingRenovateData(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer, client Client) error {
	queries := renovatedb.New(sqlDB)

	rows, err := queries.RetrieveReposAndPackagesForMissingData(ctx)
	if err != nil {
		return err
	}
	tracker := progress.Tracker{
		Message: fmt.Sprintf("Retrieving dependency details for %d Renovate dependencies", len(rows)),
		Total:   int64(len(rows)),
	}
	pw.AppendTracker(&tracker)
	defer tracker.MarkAsDone()
	go pw.Render()

	var eg errgroup.Group
	var m sync.Mutex
	goroutines := make(chan struct{}, maxGoroutines)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	for _, row := range rows {
		row := row
		eg.Go(func() error {
			goroutines <- struct{}{}
			q, ok := newRenovateDependenciesQuery(row)
			if !ok {
				logger.Debug(fmt.Sprintf("Package lookup for dependency %v skipped", row))
				tracker.Increment(1)
				<-goroutines
				return nil
			}

			resp, err, notFound := client.GetDependencies(q)
			if notFound {
				logger.Warn(fmt.Sprintf("Package lookup for dependency %v failed: not found", q))
				tracker.Increment(1)
				<-goroutines
				return nil
			}
			if err != nil {
				tracker.MarkAsErrored()
				<-goroutines
				return err
			}

			for _, dep := range resp.Nodes {
				// skip the re-definition of the dependency we're looking up
				if dep.Relation == DependenciesRelationSelf {
					continue
				}

				var depTypes []string
				err = json.Unmarshal([]byte(row.DepTypes), &depTypes)
				if err != nil {
					logger.Warn(fmt.Sprintf("Failed to unmarshal dep_types from %s for dependency %v", row.DepTypes, q), "err", err)
				}

				depTypes = append(depTypes, "missing-data")

				var encodedDepTypes []byte
				encodedDepTypes, err := json.Marshal(depTypes)
				if err != nil {
					logger.Warn(fmt.Sprintf("Failed to marshal dep_types %v for dependency %v", depTypes, q), "err", err)
				}

				m.Lock()
				err = queries.WithTx(tx).InsertPackage(ctx, renovatedb.InsertPackageParams{
					Platform:     row.Platform,
					Organisation: row.Organisation,
					Repo:         row.Repo,

					PackageName: dep.VersionKey.Name,
					Version:     dep.VersionKey.Version,
					CurrentVersion: sql.NullString{
						String: dep.VersionKey.Version,
						Valid:  true,
					},

					PackageManager:  row.PackageManager,
					PackageFilePath: row.PackageFilePath,
					Datasource:      row.Datasource,

					DepTypes: string(encodedDepTypes),
				})
				if err != nil {
					m.Unlock()
					tracker.MarkAsErrored()
					<-goroutines
					return err
				}
				m.Unlock()
			}

			tracker.Increment(1)
			<-goroutines
			return nil
		})
	}

	err = eg.Wait()
	if err != nil {
		return err
	}

	return tx.Commit()
}

func generateMissingSBOMData(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer, client Client) error {
	queries := sbomdb.New(sqlDB)

	rows, err := queries.RetrieveReposAndPackagesForMissingData(ctx)
	if err != nil {
		return err
	}
	tracker := progress.Tracker{
		Message: fmt.Sprintf("Retrieving dependency details for %d SBOM dependencies", len(rows)),
		Total:   int64(len(rows)),
	}
	pw.AppendTracker(&tracker)
	defer tracker.MarkAsDone()
	go pw.Render()

	var eg errgroup.Group
	var m sync.Mutex
	goroutines := make(chan struct{}, maxGoroutines)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	for _, row := range rows {
		row := row
		eg.Go(func() error {
			goroutines <- struct{}{}
			q, ok := newSBOMDependenciesQuery(row)
			if !ok {
				logger.Debug(fmt.Sprintf("Package lookup for dependency %v skipped", row))
				tracker.Increment(1)
				<-goroutines
				return nil
			}

			resp, err, notFound := client.GetDependencies(q)
			if notFound {
				logger.Warn(fmt.Sprintf("Package lookup for dependency %v failed: not found", q))
				tracker.Increment(1)
				<-goroutines
				return nil
			}
			if err != nil {
				tracker.MarkAsErrored()
				<-goroutines
				return err
			}

			for _, dep := range resp.Nodes {
				// skip the re-definition of the dependency we're looking up
				if dep.Relation == DependenciesRelationSelf {
					continue
				}

				m.Lock()
				err = queries.WithTx(tx).InsertPackage(ctx, sbomdb.InsertPackageParams{
					Platform:     row.Platform,
					Organisation: row.Organisation,
					Repo:         row.Repo,

					PackageName: dep.VersionKey.Name,
					Version: sql.NullString{
						String: dep.VersionKey.Version,
						Valid:  true,
					},
					CurrentVersion: sql.NullString{
						String: dep.VersionKey.Version,
						Valid:  true,
					},

					PackageType: row.PackageType,
				})
				if err != nil {
					m.Unlock()
					tracker.MarkAsErrored()
					<-goroutines
					return err
				}
				m.Unlock()
			}

			tracker.Increment(1)
			<-goroutines
			return nil
		})
	}

	err = eg.Wait()
	if err != nil {
		return err
	}

	return tx.Commit()
}
