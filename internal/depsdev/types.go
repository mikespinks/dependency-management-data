package depsdev

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/depsdev/db"
)

type DepsDev struct{}

func (d *DepsDev) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}

func (d *DepsDev) AnonymiseData(ctx context.Context, sqlDB *sql.DB, orgs []string) (err error) {
	queries := db.New(sqlDB)

	for _, org := range orgs {
		err = queries.AnonymiseCvesPackageNameByOrg(ctx, org)
		if err != nil {
			return
		}
		err = queries.AnonymiseLicensesPackageNameByOrg(ctx, org)
		if err != nil {
			return
		}
	}

	return
}
