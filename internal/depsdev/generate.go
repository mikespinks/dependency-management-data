package depsdev

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"
	"net/http"
	"sync"
	"time"

	renovatedb "dmd.tanna.dev/internal/datasources/renovate/db"
	sbomdb "dmd.tanna.dev/internal/datasources/sbom/db"
	"dmd.tanna.dev/internal/depsdev/db"
	"github.com/jedib0t/go-pretty/v6/progress"
	"golang.org/x/sync/errgroup"
)

const maxGoroutines = 50

func Generate(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer, httpClient *http.Client) error {
	client := NewClient(httpClient)

	err := generateRenovate(ctx, logger, sqlDB, pw, client)
	if err != nil {
		return err
	}

	err = generateSBOM(ctx, logger, sqlDB, pw, client)
	if err != nil {
		return err
	}

	return nil
}

func generateRenovate(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer, client Client) error {
	renovateQueries := renovatedb.New(sqlDB)

	rows, err := renovateQueries.RetrieveDistinctPackages(ctx)
	if err != nil {
		return err
	}
	tracker := progress.Tracker{
		Message: fmt.Sprintf("Retrieving dependency details for %d Renovate dependencies", len(rows)),
		Total:   int64(len(rows)),
	}
	pw.AppendTracker(&tracker)
	defer tracker.MarkAsDone()
	go pw.Render()

	var eg errgroup.Group
	var m sync.Mutex
	goroutines := make(chan int, maxGoroutines)

	queries := db.New(sqlDB)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	for _, row := range rows {
		row := row
		eg.Go(func() error {
			goroutines <- 1
			q, ok := newRenovatePackageVersionQuery(row)
			if !ok {
				logger.Debug(fmt.Sprintf("Package lookup for dependency %v skipped", row))
				tracker.Increment(1)
				<-goroutines
				return nil
			}

			resp, err, notFound := client.GetVersion(q)
			if notFound {
				logger.Warn(fmt.Sprintf("Package lookup for dependency %v failed: not found", q))
				tracker.Increment(1)
				<-goroutines
				return nil
			}
			if err != nil {
				tracker.MarkAsErrored()
				<-goroutines
				return err
			}

			for _, license := range resp.Licenses {
				m.Lock()

				err = queries.WithTx(tx).InsertLicense(ctx, db.InsertLicenseParams{
					PackageName: q.Name,
					Version:     q.Version,
					License:     license,
					UpdatedAt:   time.Now().Format(time.RFC3339),
				})
				if err != nil {
					m.Unlock()
					tracker.MarkAsErrored()
					<-goroutines
					return err
				}
				m.Unlock()
			}

			for _, adv := range resp.AdvisoryKeys {
				m.Lock()

				err = queries.WithTx(tx).InsertCve(ctx, db.InsertCveParams{
					PackageName: q.Name,
					Version:     q.Version,
					CveID:       adv.ID,
					UpdatedAt:   time.Now().Format(time.RFC3339),
				})
				if err != nil {
					m.Unlock()
					tracker.MarkAsErrored()
					<-goroutines
					return err
				}

				m.Unlock()
			}
			tracker.Increment(1)
			<-goroutines
			return nil
		})
	}

	err = eg.Wait()
	if err != nil {
		return err
	}

	return tx.Commit()
}

func generateSBOM(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer, client Client) error {
	sbomQueries := sbomdb.New(sqlDB)

	rows, err := sbomQueries.RetrieveDistinctPackages(ctx)
	if err != nil {
		return err
	}
	tracker := progress.Tracker{
		Message: fmt.Sprintf("Retrieving dependency details for %d SBOM dependencies", len(rows)),
		Total:   int64(len(rows)),
	}
	pw.AppendTracker(&tracker)
	defer tracker.MarkAsDone()
	go pw.Render()

	var eg errgroup.Group
	var m sync.Mutex
	goroutines := make(chan int, maxGoroutines)

	queries := db.New(sqlDB)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	for _, row := range rows {
		row := row
		eg.Go(func() error {
			goroutines <- 1
			q, ok := newSBOMPackageVersionQuery(row)
			if !ok {
				logger.Debug(fmt.Sprintf("Package lookup for dependency %v skipped", row))
				tracker.Increment(1)
				<-goroutines
				return nil
			}

			resp, err, notFound := client.GetVersion(q)
			if notFound {
				logger.Warn(fmt.Sprintf("Package lookup for dependency %v failed: not found", q))
				tracker.Increment(1)
				<-goroutines
				return nil
			}
			if err != nil {
				tracker.MarkAsErrored()
				<-goroutines
				return err
			}

			for _, license := range resp.Licenses {
				m.Lock()

				err = queries.WithTx(tx).InsertLicense(ctx, db.InsertLicenseParams{
					PackageName: q.Name,
					Version:     q.Version,
					License:     license,
					UpdatedAt:   time.Now().Format(time.RFC3339),
				})
				if err != nil {
					m.Unlock()
					tracker.MarkAsErrored()
					<-goroutines
					return err
				}
				m.Unlock()
			}

			for _, adv := range resp.AdvisoryKeys {
				m.Lock()

				err = queries.WithTx(tx).InsertCve(ctx, db.InsertCveParams{
					PackageName: q.Name,
					Version:     q.Version,
					CveID:       adv.ID,
					UpdatedAt:   time.Now().Format(time.RFC3339),
				})
				if err != nil {
					m.Unlock()
					tracker.MarkAsErrored()
					<-goroutines
					return err
				}

				m.Unlock()
			}
			tracker.Increment(1)
			<-goroutines
			return nil
		})
	}

	err = eg.Wait()
	if err != nil {
		return err
	}

	return tx.Commit()
}
