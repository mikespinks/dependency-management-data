package endoflifedate

import (
	"fmt"
	"log/slog"
	"regexp"

	"dmd.tanna.dev/internal/endoflifedate/db"
)

func init() {
	renovateParsers = append(renovateParsers, &renovateParserAlpine{})
}

type renovateParserAlpine struct{}

func (*renovateParserAlpine) Handled(dep db.RetrieveDistinctRenovateDepsRow) bool {
	return dep.PackageName == "alpine"
}

func (*renovateParserAlpine) ParseProductAndCycle(logger *slog.Logger, dep db.RetrieveDistinctRenovateDepsRow) (p string, c string, ok bool) {
	p = "alpine"

	r_ := regexp.MustCompile("[v >=<~^]*")
	version := r_.ReplaceAllString(getDepVersion(dep.Version, dep.CurrentVersion), "")

	match, _ := regexp.MatchString("^[0-9]+$", version)
	if match {
		logger.Warn(fmt.Sprintf("Couldn't assume cycle of %s that %s corresponds to", p, dep.DependencyDetails()))
		return
	}

	// i.e. 3.14
	match, _ = regexp.MatchString("^[0-9]+\\.[0-9]+", version)
	if match {
		cycle := majorDotMinor(version)

		return p, cycle, true
	}

	return
}
