package endoflifedate

import (
	"regexp"

	"dmd.tanna.dev/internal/endoflifedate/db"
	"log/slog"
)

func init() {
	renovateParsers = append(renovateParsers, &renovateParserNodejs{})
}

type renovateParserNodejs struct{}

func (*renovateParserNodejs) Handled(dep db.RetrieveDistinctRenovateDepsRow) bool {
	return (dep.PackageName == "node" && dep.Datasource != "orb") || dep.PackageManager == "nodenv"
}

func (*renovateParserNodejs) ParseProductAndCycle(logger *slog.Logger, dep db.RetrieveDistinctRenovateDepsRow) (p string, c string, ok bool) {
	p = "nodejs"

	r_ := regexp.MustCompile("[v >=<~^]*")
	version := r_.ReplaceAllString(getDepVersion(dep.Version, dep.CurrentVersion), "")

	match, _ := regexp.MatchString("^[0-9]+\\.[0-9]+\\.[0-9]+", version)
	if match {
		cycle := major(version)

		return p, cycle, true
	}

	// i.e. 18.0
	// i.e. 18.x
	match, _ = regexp.MatchString("^[0-9]+\\.[0-9a-z]+", version)
	if match {
		cycle := major(version)

		return p, cycle, true
	}

	// i.e. 18-alpine
	match, _ = regexp.MatchString("^[0-9]+-", version)
	if match {
		cycle := major(version)

		return p, cycle, true
	}

	return
}
