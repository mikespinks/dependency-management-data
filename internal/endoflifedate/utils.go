package endoflifedate

import "database/sql"

func getDepVersion(version string, currentVersion sql.NullString) string {
	if currentVersion.Valid {
		return currentVersion.String
	}
	return version
}

func getSBOMDepVersion(version sql.NullString, currentVersion sql.NullString) string {
	if currentVersion.Valid {
		return currentVersion.String
	}
	if version.Valid {
		return version.String
	}
	return "" // shouldn't ever happen, as we should filter out any dependencies with nullables
}
