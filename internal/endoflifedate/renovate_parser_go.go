package endoflifedate

import (
	"fmt"
	"strings"

	"dmd.tanna.dev/internal/endoflifedate/db"
	"log/slog"
)

func init() {
	renovateParsers = append(renovateParsers, &renovateParserGo{})
}

type renovateParserGo struct{}

func (*renovateParserGo) Handled(dep db.RetrieveDistinctRenovateDepsRow) bool {
	return dep.PackageName == "go" || dep.PackageName == "golang"
}

func (*renovateParserGo) ParseProductAndCycle(logger *slog.Logger, dep db.RetrieveDistinctRenovateDepsRow) (p string, c string, ok bool) {
	p = "go"

	// note that this must use dep.Version as dep.CurrentVersion will resolve to the version of Go that is being used when running renovate-graph, whereas dep.Version matches the version set in the `go.mod`
	parts := strings.Split(dep.Version, ".")
	if len(parts) < 2 {
		return
	}

	cycle := fmt.Sprintf("%s.%s", parts[0], trimDockerSuffix(parts[1]))

	return p, cycle, true
}
