package endoflifedate

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/endoflifedate/db"
)

func (*EndOfLifeDate) AnonymiseData(ctx context.Context, sqlDB *sql.DB, orgs []string) (err error) {
	db := db.New(sqlDB)

	for _, org := range orgs {
		err = db.AnonymiseRenovatePackageNameByOrg(ctx, org)
		if err != nil {
			return
		}

		err = db.AnonymiseRenovateVersionByOrg(ctx, org)
		if err != nil {
			return
		}
	}

	err = db.AnonymiseDependabotOrgAndRepo(ctx)
	if err != nil {
		return
	}

	for _, org := range orgs {
		err = db.AnonymiseSBOMPackageNameByOrg(ctx, org)
		if err != nil {
			return
		}

		err = db.AnonymiseSBOMVersionByOrg(ctx, sql.NullString{
			Valid:  true,
			String: org,
		})
		if err != nil {
			return
		}

		err = db.AnonymiseSBOMCurrentVersionByOrg(ctx, sql.NullString{
			Valid:  true,
			String: org,
		})

		if err != nil {
			return
		}
	}

	return nil
}
