package endoflifedate

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"log/slog"

	"dmd.tanna.dev/internal/endoflifedate/client"
	"dmd.tanna.dev/internal/endoflifedate/db"
)

type SBOM struct {
	sqlDB    *sql.DB
	queries  db.Queries
	client   *client.ClientWithResponses
	logger   *slog.Logger
	products map[string][]client.Cycle
}

func NewSBOM(sqlDB *sql.DB, theClient *client.ClientWithResponses, logger *slog.Logger) SBOM {
	return SBOM{
		sqlDB:    sqlDB,
		queries:  *db.New(sqlDB),
		client:   theClient,
		logger:   logger,
		products: make(map[string][]client.Cycle),
	}
}

func (r SBOM) ProcessDependency(ctx context.Context, tx *sql.Tx, dep db.RetrieveDistinctSBOMDepsRow) error {
	product, cycle, ok := r.RetrieveEOLStatus(ctx, dep)
	if !ok {
		r.logger.Debug(fmt.Sprintf("Could not parse dependency %v as an endoflife.date Product/Cycle, returned (%v, %v, %v)", dep, product, cycle, ok))
		return nil
	}

	if cycle.Cycle == "" {
		r.logger.Debug(fmt.Sprintf("Could not parse dependency %v as an endoflife.date Cycle, returned (%v, %v, %v)", dep, product, cycle, ok))
		return nil
	}

	productCycleParams := db.InsertProductCycleParams{
		ProductName:    product.Name,
		Cycle:          cycle.Cycle,
		EolFrom:        emptyOrNullString(cycle.EolFrom),
		SupportedUntil: emptyOrNullString(cycle.SupportedUntil),
		InsertedAt:     time.Now().Format(time.RFC3339),
	}

	err := r.queries.WithTx(tx).InsertProductCycle(ctx, productCycleParams)
	if err != nil {
		return err
	}

	renovateParams := db.InsertSBOMEndOfLifeParams{
		PackageName:    dep.PackageName,
		Version:        dep.Version,
		CurrentVersion: dep.CurrentVersion,
		PackageType:    dep.PackageType,
		ProductName:    product.Name,
		Cycle:          cycle.Cycle,
	}

	err = r.queries.WithTx(tx).InsertSBOMEndOfLife(ctx, renovateParams)
	if err != nil {
		return err
	}

	return nil
}

func (r SBOM) retrieveProductInformation(ctx context.Context, dep db.RetrieveDistinctSBOMDepsRow, product string) ([]client.Cycle, error) {
	cycles, ok := r.products[product]
	if ok {
		r.logger.Debug(fmt.Sprintf("Using cached endoflife.date product %s with %d cycles", product, len(cycles)))
		return cycles, nil
	}

	r.logger.Debug(fmt.Sprintf("Looking up endoflife.date product information for %v for dependency %v", product, dep))
	resp, err := r.client.GetApiProductJsonWithResponse(ctx, product)
	if err != nil {
		return nil, err
	}

	r.logger.Debug(fmt.Sprintf("Received an HTTP %d from endoflife.date product information for %s for dependency %v", resp.StatusCode(), product, dep))
	if resp.JSON200 == nil {
		return nil, fmt.Errorf("Received an HTTP %d from endoflife.date, with no JSON200 response body", resp.StatusCode())
	}

	r.products[product] = *resp.JSON200
	return r.products[product], nil
}

func (r SBOM) RetrieveEOLStatus(ctx context.Context, dep db.RetrieveDistinctSBOMDepsRow) (p Product, c Cycle, ok bool) {
	product, cycle, ok := r.parseProductAndCycle(dep)
	if !ok {
		return
	}

	cycles, err := r.retrieveProductInformation(ctx, dep, product)
	if err != nil {
		r.logger.Warn(fmt.Sprintf("Received an error while looking up %s's data in endoflife.date: %v", product, err))
		return
	}
	p.Name = product

	for _, cy := range cycles {
		if cy.Cycle != nil {
			cycleStr, err := cy.Cycle.AsCycleCycle0()
			if err == nil {
				if cycle == cycleStr {
					ok = true
					c.Cycle = cycleStr

					if cy.Eol != nil {
						eol, err := cy.Eol.AsCycleEol0()
						if err == nil {
							c.EolFrom = eol
						}
					}

					if cy.Support != nil {
						supported, err := cy.Support.AsCycleSupport0()
						if err == nil {
							c.SupportedUntil = supported.Format("2006-01-02")
						}
					}
				}
			}
		}
	}

	return
}

func (r SBOM) parseProductAndCycle(dep db.RetrieveDistinctSBOMDepsRow) (p string, c string, ok bool) {
	if dep.Version.Valid && dep.Version.String == "latest" ||
		dep.CurrentVersion.Valid && dep.CurrentVersion.String == "latest" {
		return
	}

	for _, sp := range sbomParsers {
		if sp.Handled(dep) {
			p, c, ok = sp.ParseProductAndCycle(r.logger, dep)
			if ok {
				return
			}
		}
	}

	return
}
