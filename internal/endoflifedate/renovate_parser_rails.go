package endoflifedate

import (
	"log/slog"
	"regexp"

	"dmd.tanna.dev/internal/endoflifedate/db"
)

func init() {
	renovateParsers = append(renovateParsers, &renovateParserRails{})
}

type renovateParserRails struct{}

func (*renovateParserRails) Handled(dep db.RetrieveDistinctRenovateDepsRow) bool {
	return dep.PackageName == "rails" && dep.PackageManager == "bundler"
}

func (*renovateParserRails) ParseProductAndCycle(logger *slog.Logger, dep db.RetrieveDistinctRenovateDepsRow) (p string, c string, ok bool) {
	p = "rails"

	version := getDepVersion(dep.Version, dep.CurrentVersion)
	match, _ := regexp.MatchString("^[0-9]+\\.[0-9]+", version)
	if match {
		cycle := majorDotMinor(version)

		return p, cycle, true
	}
	return
}
