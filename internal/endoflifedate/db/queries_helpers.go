package db

import "fmt"

func (d RetrieveDistinctRenovateDepsRow) DependencyDetails() string {
	if d.CurrentVersion.Valid {
		return fmt.Sprintf("(%s@%s/%s)", d.PackageName, d.Version, d.CurrentVersion.String)
	}
	return fmt.Sprintf("(%s@%s/<nil>)", d.PackageName, d.Version)
}
