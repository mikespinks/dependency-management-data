-- name: InsertProductCycle :exec
insert into endoflifedate_products (
  product_name,
  cycle,
  supported_until,
  eol_from,
  inserted_at
  ) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?
);

------------ Renovate

-- name: RetrieveDistinctRenovateDeps :many
select distinct package_name, version, current_version, package_manager, datasource from renovate;

-- name: InsertRenovateEndOfLife :exec
insert into renovate_endoflife (
  package_name,
  version,
  current_version,

  package_manager,

  datasource,

  product_name,
  cycle
  ) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);

-- name: AnonymiseRenovatePackageNameByOrg :exec
update renovate_endoflife
set
  package_name  = 'ANON-' || hex(sha3(package_name))
where
    package_name like '%' || ? || '%'
;

-- name: AnonymiseRenovateVersionByOrg :exec
update renovate_endoflife
set
  version  = 'ANON-' || hex(sha3(version))
where
    version like '%' || ? || '%'
;

------------ Dependabot

-- name: InsertDependabotEndOfLife :exec
insert into dependabot_endoflife (
  package_name,
  version,

  package_manager,

  product_name,
  cycle
  ) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?
);

-- name: AnonymiseDependabotOrgAndRepo :exec
update dependabot_endoflife
set
  organisation  = 'ANON-' || hex(sha3(organisation)),
  repo          = 'ANON-' || hex(sha3(repo))
where
  organisation
    NOT LIKE 'ANON-%'
  OR
  repo
    NOT LIKE 'ANON-%'
;

------------ SBOMs

-- name: RetrieveDistinctSBOMDeps :many
select distinct
  package_name,
  version,
  current_version,
  package_type
from
sboms
where
  version IS NOT NULL
  AND
  current_version IS NOT NULL
;

-- name: InsertSBOMEndOfLife :exec
insert into sboms_endoflife (
  package_name,
  version,
  current_version,
  package_type,

  product_name,
  cycle
  ) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);

-- name: AnonymiseSBOMOrgAndRepo :exec
update sboms_endoflife
set
  organisation  = 'ANON-' || hex(sha3(organisation)),
  repo          = 'ANON-' || hex(sha3(repo))
where
  organisation
    NOT LIKE 'ANON-%'
  OR
  repo
    NOT LIKE 'ANON-%'
;

-- name: AnonymiseSBOMPackageNameByOrg :exec
update sboms_endoflife
set
  package_name  = 'ANON-' || hex(sha3(package_name))
where
    package_name like '%' || ? || '%'
;

-- name: AnonymiseSBOMVersionByOrg :exec
update sboms_endoflife
set
  version  = 'ANON-' || hex(sha3(version))
where
    version IS NOT NULL
    and
    version like '%' || ? || '%'
;

-- name: AnonymiseSBOMCurrentVersionByOrg :exec
update sboms_endoflife
set
  current_version  = 'ANON-' || hex(sha3(current_version))
where
    current_version IS NOT NULL
    and
    current_version like '%' || ? || '%'
;
