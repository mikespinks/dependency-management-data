-- endoflifedate_products contains the underlying data model for EndOfLife.date products + cycles that can be JOINed with the `*_endoflife` tables
CREATE TABLE IF NOT EXISTS endoflifedate_products (
  -- from Product

  product_name TEXT NOT NULL,

  -- from Cycle

  cycle TEXT NOT NULL,

  supported_until TEXT,
  eol_from TEXT,

  inserted_at TEXT NOT NULL,

  UNIQUE (product_name, cycle) ON CONFLICT REPLACE,
  CHECK(product_name <> ''),
  CHECK(cycle <> '')
);

-- renovate_endoflife contains mappings for the `renovate` datasource that can be JOINed with `endoflifedate_products` and `renovate` to provide the resulting set of renovate dependencies that are End Of Life - See the `RetrievePackageAdvisories` query in `advisory/db/queries.sql`
CREATE TABLE IF NOT EXISTS renovate_endoflife (
  -- from renovate
  package_name TEXT NOT NULL,
  version TEXT NOT NULL,
  current_version TEXT,

  package_manager TEXT NOT NULL,

  datasource TEXT NOT NULL,

  -- from endoflifedate_products
  product_name TEXT NOT NULL,
  cycle TEXT NOT NULL,

  UNIQUE (package_name, version, current_version, package_manager, datasource) ON CONFLICT REPLACE
);

-- dependabot_endoflife contains mappings for the `dependabot` datasource that can be JOINed with `endoflifedate_products` and `dependabot` to provide the resulting set of dependabot dependencies that are End Of Life
CREATE TABLE IF NOT EXISTS dependabot_endoflife (
  -- from dependabot
  package_name TEXT NOT NULL,
  version TEXT NOT NULL,

  package_manager TEXT NOT NULL,

  -- from endoflifedate_products
  product_name TEXT NOT NULL,
  cycle TEXT NOT NULL,

  UNIQUE (package_name, version, package_manager) ON CONFLICT REPLACE
);

-- sbom_endoflife contains mappings for the `sboms` datasource that can be JOINed with `endoflifedate_products` and `sboms` to provide the resulting set of SBOM dependencies that are End Of Life
CREATE TABLE IF NOT EXISTS sboms_endoflife (
  -- from sboms
  package_name TEXT NOT NULL,
  version TEXT,
  current_version TEXT,
  package_type TEXT NOT NULL,

  -- from endoflifedate_products
  product_name TEXT NOT NULL,
  cycle TEXT NOT NULL,

  UNIQUE (package_name, version, package_type) ON CONFLICT REPLACE
);
