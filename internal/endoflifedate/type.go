package endoflifedate

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/endoflifedate/db"
)

type EndOfLifeDate struct{}

func (*EndOfLifeDate) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
