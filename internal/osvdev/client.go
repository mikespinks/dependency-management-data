package osvdev

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

const baseURL = "https://api.osv.dev/v1"

type Client struct {
	httpClient *http.Client
}

func NewClient(httpClient *http.Client) Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	return Client{
		httpClient: httpClient,
	}
}

type QueryResponse struct {
	Vulns []Vuln `json:"vulns"`
}

func (c *Client) Query(q Query) (vulns Vulns, err error) {
	bodyBytes, err := json.Marshal(q)
	if err != nil {
		return
	}
	resp, err := c.httpClient.Post(baseURL+"/query", "application/json", bytes.NewReader(bodyBytes))
	if err != nil {
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return vulns, fmt.Errorf("received an HTTP %d response query Querying %v", resp.StatusCode, q)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	var b QueryResponse
	err = json.Unmarshal(body, &b)
	if err != nil {
		return
	}

	vulns.Query = q
	vulns.Vulns = b.Vulns

	return vulns, nil
}

type QueryBatchRequest struct {
	Queries []Query `json:"queries"`
}

type QueryBatchVuln struct {
	ID string `json:"id"`
}

type QueryBatchResponse struct {
	Results []struct {
		Vulns []QueryBatchVuln `json:"vulns"`
	} `json:"results"`
}

func (c *Client) QueryBatch(q []Query) ([]Vulns, error) {
	if len(q) > 1000 {
		return nil, fmt.Errorf("QueryBatch allows a maximum of 1000 dependencies")
	}
	req := QueryBatchRequest{
		Queries: q,
	}
	bodyBytes, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	resp, err := c.httpClient.Post(baseURL+"/querybatch", "application/json", bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("received an HTTP %d response query Querying %v", resp.StatusCode, q)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var b QueryBatchResponse
	err = json.Unmarshal(body, &b)
	if err != nil {
		return nil, err
	}

	vulns := make([]Vulns, len(b.Results))
	for i, v := range b.Results {
		vulns[i] = Vulns{
			Query: q[i],
		}
		vulns[i].Vulns = make([]Vuln, len(v.Vulns))
		for j, vuln := range v.Vulns {
			vulns[i].Vulns[j] = Vuln{
				ID: vuln.ID,
			}
		}
	}

	return vulns, nil
}
