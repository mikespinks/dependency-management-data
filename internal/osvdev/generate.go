package osvdev

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	renovatedb "dmd.tanna.dev/internal/datasources/renovate/db"
	"dmd.tanna.dev/internal/osvdev/db"

	"github.com/jedib0t/go-pretty/v6/progress"
)

func Generate(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer, httpClient *http.Client) error {
	client := NewClient(httpClient)

	err := generateRenovate(ctx, logger, sqlDB, pw, client)
	if err != nil {
		return err
	}

	// Note that Dependabot isn't yet supported https://gitlab.com/tanna.dev/dependency-management-data/-/issues/57
	return nil
}

func generateRenovate(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer, client Client) error {
	renovateQueries := renovatedb.New(sqlDB)
	deps, err := renovateQueries.RetrieveDistinctPackages(ctx)
	if err != nil {
		return err
	}
	tracker := progress.Tracker{
		Message: fmt.Sprintf("Checking CVE status for %d Renovate dependencies", len(deps)),
		Total:   int64(len(deps)),
	}
	pw.AppendTracker(&tracker)
	defer tracker.MarkAsDone()
	go pw.Render()

	batches := chunkSlice(deps, 1000)

	queries := db.New(sqlDB)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	for _, deps := range batches {
		deps := deps
		var qs []Query

		for _, d := range deps {
			q, ok := NewRenovateQuery(d)
			if ok {
				qs = append(qs, q)
			}
		}

		vulns, err := client.QueryBatch(qs)
		if err != nil {
			tracker.MarkAsErrored()
			return err
		}

		for _, v := range vulns {
			for _, vuln := range v.Vulns {
				params := db.InsertCveParams{
					PackageName: v.Query.Package.Name,
					Version:     v.Query.Version,
					CveID:       vuln.ID,
					UpdatedAt:   time.Now().Format(time.RFC3339),
				}

				err = queries.WithTx(tx).InsertCve(ctx, params)

				if err != nil {
					tracker.MarkAsErrored()
					return err
				}
			}
		}
		tracker.Increment(int64(len(deps)))
	}

	return tx.Commit()
}

func queryBatch(ctx context.Context, client Client, deps []renovatedb.RetrieveDistinctPackagesRow, vulnsCh chan<- []Vulns) error {
	var qs []Query
	for _, d := range deps {
		q, ok := NewRenovateQuery(d)
		if ok {
			qs = append(qs, q)
		}
	}

	vulns, err := client.QueryBatch(qs)
	if err != nil {
		return err
	}

	vulnsCh <- vulns

	return nil
}

// adapted MIT from https://freshman.tech/snippets/go/split-slice-into-chunks/
func chunkSlice[T any](slice []T, chunkSize int) [][]T {
	var chunks [][]T
	for i := 0; i < len(slice); i += chunkSize {
		end := i + chunkSize

		// necessary check to avoid slicing beyond
		// slice capacity
		if end > len(slice) {
			end = len(slice)
		}

		chunks = append(chunks, slice[i:end])
	}

	return chunks
}
