-- name: InsertCve :exec
INSERT INTO osvdev_cves (
  package_name,
  version,

  cve_id,

  updated_at
  ) VALUES (
  ?,
  ?,

  ?,

  ?
);

-- name: AnonymisePackageNameByOrg :exec
update osvdev_cves
set
  package_name  = 'ANON-' || hex(sha3(package_name))
where
    package_name like '%' || ? || '%'
;
