CREATE TABLE IF NOT EXISTS osvdev_cves (
  package_name TEXT NOT NULL,
  version TEXT NOT NULL,

  cve_id TEXT NOT NULL,

  updated_at TEXT NOT NULL,

  UNIQUE (package_name, version, cve_id) ON CONFLICT REPLACE
);
