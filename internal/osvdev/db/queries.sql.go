// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.21.0
// source: queries.sql

package db

import (
	"context"
)

const anonymisePackageNameByOrg = `-- name: AnonymisePackageNameByOrg :exec
update osvdev_cves
set
  package_name  = 'ANON-' || hex(sha3(package_name))
where
    package_name like '%' || ? || '%'
`

func (q *Queries) AnonymisePackageNameByOrg(ctx context.Context, packageName string) error {
	_, err := q.db.ExecContext(ctx, anonymisePackageNameByOrg, packageName)
	return err
}

const insertCve = `-- name: InsertCve :exec
INSERT INTO osvdev_cves (
  package_name,
  version,

  cve_id,

  updated_at
  ) VALUES (
  ?,
  ?,

  ?,

  ?
)
`

type InsertCveParams struct {
	PackageName string
	Version     string
	CveID       string
	UpdatedAt   string
}

func (q *Queries) InsertCve(ctx context.Context, arg InsertCveParams) error {
	_, err := q.db.ExecContext(ctx, insertCve,
		arg.PackageName,
		arg.Version,
		arg.CveID,
		arg.UpdatedAt,
	)
	return err
}
