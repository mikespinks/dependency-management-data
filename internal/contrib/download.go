package contrib

import (
	"archive/zip"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/xanzy/go-gitlab"
)

func ContribPath() string {
	return os.ExpandEnv("$HOME/.cache/dmd/contrib")
}

// DownloadContrib downloads the latest archive from the -contrib repository to ContribPath()'s location
func DownloadContrib(glab *gitlab.Client, destination string) error {
	tmpFile, err := os.CreateTemp("", "dmd-contrib-download")
	if err != nil {
		return err
	}
	defer tmpFile.Close()
	defer os.Remove(tmpFile.Name())

	_, err = glab.Repositories.StreamArchive("tanna.dev/dependency-management-data-contrib", tmpFile, &gitlab.ArchiveOptions{
		Format: gitlab.String("zip"),
	})
	if err != nil {
		return err
	}

	zrc, err := zip.OpenReader(tmpFile.Name())
	if err != nil {
		return err
	}

	defer zrc.Close()

	// archives contain a top-level directory called i.e. dependency-management-data-contrib-main-d1936fa65d9311b820a2ad25392a1785ed18d97d/ that we want to strip
	base := ""

	// adapted from https://golang.cafe/blog/golang-unzip-file-example.html
	for i, f := range zrc.File {
		if i == 0 {
			base = f.Name
			continue
		}

		filePath := filepath.Join(destination, strings.ReplaceAll(f.Name, base, ""))

		if !strings.HasPrefix(filePath, filepath.Clean(destination)+string(os.PathSeparator)) {
			return fmt.Errorf("invalid file path: %s\n", filePath)
		}

		if f.FileInfo().IsDir() {
			err = os.MkdirAll(filePath, os.ModePerm)
			if err != nil {
				return err
			}
			continue
		}

		if err := os.MkdirAll(filepath.Dir(filePath), os.ModePerm); err != nil {
			return err
		}

		dstFile, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {
			return err
		}

		fileInArchive, err := f.Open()
		if err != nil {
			return err
		}

		if _, err := io.Copy(dstFile, fileInArchive); err != nil {
			return err
		}

		err = dstFile.Close()
		if err != nil {
			return err
		}
		err = fileInArchive.Close()
		if err != nil {
			return err
		}
	}
	return nil
}
