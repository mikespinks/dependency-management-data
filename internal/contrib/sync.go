package contrib

import (
	"context"
	"database/sql"
	"fmt"
	"log/slog"
	"os"
	"path"

	"github.com/jedib0t/go-pretty/v6/progress"
)

func Sync(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer) error {
	err := GenerateAdvisories(ctx, logger, sqlDB, pw)
	if err != nil {
		return err
	}

	return nil
}

func GenerateAdvisories(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer) error {
	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	contribPath := ContribPath()
	_, err = os.Stat(contribPath)
	if err != nil {
		logger.Warn(fmt.Sprintf("No contrib directory found at %s - have you run `dmd contrib download`?", contribPath))
		return nil
	}

	advisoriesPath := path.Join(contribPath, "advisories")

	_, err = os.Stat(advisoriesPath)
	if err != nil {
		logger.Warn(fmt.Sprintf("No advisories directory found in contribPath: %s - have you run `dmd contrib download`?", contribPath))
		return nil
	}

	files, err := os.ReadDir(advisoriesPath)
	if err != nil {
		return fmt.Errorf("could not read the advisories directory (%s): %v", advisoriesPath, err)
	}

	for _, de := range files {
		contents, err := os.ReadFile(path.Join(advisoriesPath, de.Name()))
		if err != nil {
			return err
		}

		_, err = tx.Exec(string(contents))
		if err != nil {
			return err
		}
	}

	return tx.Commit()
}
