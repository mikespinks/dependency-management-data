package advisory

// Advisory contains a package advisory, which allows flagging packages (for an optional set of version(s)) for arbitrary reasons
type Advisory struct {
	// PackagePattern contains a simple wildcard-aware pattern of packages that these advisories are for
	// For example:
	// `github.com/gorilla/*` would expect to match `github.com/gorilla/mux` and `github.com/gorilla/`
	// `git*.com/jamietanna/*` would expect to match `gitlab.com/jamietanna/test` and `github.com/jamietanna/not-test`
	PackagePattern string
	PackageManager string
	// Version contains the version that is expected to match. If nil, will always match
	Version *string
	// VersionMatchStrategy:
	//
	// VersionMatchStrategy defines the way that the version is expected to lexicographically match. If nil, will always match
	VersionMatchStrategy *VersionMatchStrategy
	AdvisoryType         AdvisoryType
	// Description describes why a given advisory is set on this package
	Description string
}
