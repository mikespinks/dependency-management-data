CREATE TABLE IF NOT EXISTS advisories (
  package_pattern TEXT NOT NULL,
  package_manager TEXT NOT NULL,
  version TEXT,
  -- lexicographically match
  version_match_strategy TEXT
    CHECK (
      version_match_strategy IN (
        "ANY",
        "EQUALS",
        "LESS_THAN",
        "LESS_EQUAL",
        "GREATER_THAN",
        "GREATER_EQUAL"
      )
    ),
  advisory_type TEXT NOT NULL
    CHECK (
      advisory_type IN (
        "DEPRECATED",
        "UNMAINTAINED",
        "SECURITY",
        "OTHER"
      )
    ),
  description TEXT NOT NULL,

  UNIQUE (package_pattern, package_manager, version, version_match_strategy, advisory_type) ON CONFLICT REPLACE
);
