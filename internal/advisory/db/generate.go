package db

//go:generate go run dmd.tanna.dev/cmd/table-joiner schema.sql ../../datasources/renovate/db/schema.sql ../../datasources/sbom/db/schema.sql ../../endoflifedate/db/schema.sql ../../datasources/awslambda/db/schema.sql ../../datasources/awsrds/db/schema.sql ../../datasources/awselasticache/db/schema.sql ../../ownership/db/schema.sql ../../depsdev/db/schema.sql
//go:generate go run github.com/sqlc-dev/sqlc/cmd/sqlc generate
