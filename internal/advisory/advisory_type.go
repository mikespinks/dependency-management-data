package advisory

// AdvisoryType is the type of package advisory that is present
type AdvisoryType = string

const (
	AdvisoryTypeDeprecated   = "DEPRECATED"
	AdvisoryTypeUnmainatined = "UNMAINTAINED"
	AdvisoryTypeSecurity     = "SECURITY"
	AdvisoryTypeOther        = "OTHER"
)
