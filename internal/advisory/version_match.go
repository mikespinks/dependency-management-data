package advisory

// VersionMatchStrategy defines how the Advisory.Version, if present, should be compared lexicographically with a CurrentVersion
type VersionMatchStrategy = string

const (
	VersionMatchStrategyAny              = "ANY"
	VersionMatchStrategyEquals           = "EQUALS"
	VersionMatchStrategyLessThan         = "LESS_THAN"
	VersionMatchStrategyLessOrEqualTo    = "LESS_EQUAL"
	VersionMatchStrategyGreaterThan      = "GREATER_THAN"
	VersionMatchStrategyGreaterOrEqualTo = "GREATER_EQUAL"
)
