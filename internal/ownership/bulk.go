package ownership

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"dmd.tanna.dev/internal/datasources/queries"
	"dmd.tanna.dev/internal/ownership/db"
)

const wildcard = "*"

type SetBulkOwnershipParams struct {
	// Platform can be wildcarded such as "*", "renovate-*" or "*tanna*"
	Platform string
	// Organisation can be wildcarded such as "*", "renovate-*" or "*tanna*"
	Organisation string
	// Repo can be wildcarded such as "*", "renovate-*" or "*tanna*"
	Repo string

	OwnerName string
	Notes     string
}

func SetBulkOwnership(ctx context.Context, params SetBulkOwnershipParams, sqlDB *sql.DB) error {
	if len(projectQueriers) == 0 {
		return fmt.Errorf("no DistinctProjectsQuerier were available for SetOwnership")
	}

	if params.Platform == "" && params.Organisation == "" && params.Repo == "" {
		return fmt.Errorf("no `platform`, `organisation` or `repo`s were specified to filter down ownership. At least one must be set")
	}

	var qParams queries.DistinctProjectParams
	if params.Platform != "" {
		qParams.Platform = &params.Platform
	}
	if params.Organisation != "" {
		qParams.Organisation = &params.Organisation
	}
	if params.Repo != "" {
		qParams.Repo = &params.Repo
	}

	var projects []queries.DistinctProject

	for _, querier := range projectQueriers {
		all, err := querier.QueryDistinctProjects(ctx, sqlDB, qParams)
		if err != nil {
			return err
		}

		projects = append(projects, all...)
	}

	queries := db.New(sqlDB)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	for _, project := range projects {
		p := db.InsertOwnerParams{
			Platform:     project.Platform,
			Organisation: project.Organisation,
			Repo:         project.Repo,
			Owner:        params.OwnerName,
			UpdatedAt:    time.Now().Format(time.RFC3339),
		}

		if params.Notes != "" {
			p.Notes = sql.NullString{
				String: params.Notes,
				Valid:  true,
			}
		}

		err := queries.WithTx(tx).InsertOwner(ctx, p)
		if err != nil {
			return err
		}
	}

	return tx.Commit()
}
