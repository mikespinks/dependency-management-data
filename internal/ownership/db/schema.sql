CREATE TABLE IF NOT EXISTS owners (
  platform TEXT NOT NULL,
  organisation TEXT NOT NULL,
  repo TEXT NOT NULL,

  -- free-form information about who the owner is - could be an email address, a team name, Slack channel name, etc
  owner TEXT NOT NULL,
  -- free-form information for additional context, such as a link to a Slack channel, Confluence page, etc
  notes TEXT,

  updated_at TEXT NOT NULL,

  UNIQUE (platform, organisation, repo) ON CONFLICT REPLACE
);
