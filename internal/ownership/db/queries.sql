-- name: InsertOwner :exec
INSERT INTO owners (
  platform,
  organisation,
  repo,

  owner,
  notes,

  updated_at
  ) VALUES (
  ?,
  ?,
  ?,

  ?,
  ?,

  ?
);

-- name: AnonymiseOrgAndRepo :exec
update owners
set
  organisation  = 'ANON-' || hex(sha3(organisation)),
  repo          = 'ANON-' || hex(sha3(repo)),
  owner         = 'ANON-' || hex(sha3(owner)),
  notes         = 'ANON-' || hex(sha3(notes))
where
  organisation
    NOT LIKE 'ANON-%'
  OR
  repo
    NOT LIKE 'ANON-%'
;
