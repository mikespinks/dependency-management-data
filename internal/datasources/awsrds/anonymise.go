package awsrds

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/awsrds/db"
)

func (*AWSRds) AnonymiseData(ctx context.Context, sqlDB *sql.DB, orgs []string) (err error) {
	rdsDB := db.New(sqlDB)
	err = rdsDB.AnonymiseRdsDatabases(ctx)
	return err
}
