-- name: InsertDatabase :exec
INSERT INTO aws_rds_databases (
  account_id,
  region,
  arn,
  name,
  engine,
  engine_version,
  tags
) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);

-- name: RetrieveAll :many
select * from aws_rds_databases;

-- name: AnonymiseRdsDatabases :exec
update aws_rds_databases
set
  account_id  = 'ANON-' || hex(sha3(account_id)),
  arn  = 'ANON-' || hex(sha3(arn)),
  name  = 'ANON-' || hex(sha3(name)),
  tags          = 'ANON-' || hex(sha3(tags))
where
  account_id
    NOT LIKE 'ANON-%'
  OR
  arn
    NOT LIKE 'ANON-%'
  OR
  name
    NOT LIKE 'ANON-%'
  OR
  tags
    NOT LIKE 'ANON-%'
;

-- name: InsertDatabaseEngine :exec
INSERT INTO aws_rds_databases_engines (
  engine,
  engine_version,
  deprecation
) VALUES (
  ?,
  ?,
  ?
);

-- name: RetrieveAllWithDeprecation :many
select
  arn,
  name,
  db.engine,
  db.engine_version,
  deprecation
from
  aws_rds_databases as db
  natural join aws_rds_databases_engines
;
