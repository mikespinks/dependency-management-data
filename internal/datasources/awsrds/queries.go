package awsrds

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/awsrds/db"
)

func RetrieveAll(ctx context.Context, sqlDB *sql.DB) ([]Database, error) {
	queries := db.New(sqlDB)

	rows, err := queries.RetrieveAll(ctx)
	if err != nil {

		return nil, err
	}

	databases := make([]Database, len(rows))

	for i, r := range rows {
		databases[i] = newDatabaseFromDB(r)
	}

	return databases, nil
}
