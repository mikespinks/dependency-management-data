package awsrds

import (
	"context"
	"database/sql"
	"log/slog"
	"time"

	"dmd.tanna.dev/internal/datasources/awsrds/db"
	"github.com/jedib0t/go-pretty/v6/progress"
	"gitlab.com/tanna.dev/endoflife-checker/awsrds/engines"
)

func GenerateEndOfLife(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer) error {
	d := db.New(sqlDB)
	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	data := engines.DeprecationData()

	for k, engineData := range data {
		for version, di := range engineData {
			params := db.InsertDatabaseEngineParams{
				Engine:        k,
				EngineVersion: version,
				Deprecation:   di.Deprecation.Format(time.RFC3339),
			}
			err := d.WithTx(tx).InsertDatabaseEngine(ctx, params)
			if err != nil {
				return err
			}
		}
	}

	return tx.Commit()
}
