package awsrds

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sync"

	"github.com/jedib0t/go-pretty/v6/progress"
	"gitlab.com/tanna.dev/endoflife-checker/awsrds"
)

type parser struct{}

func NewParser() parser {
	return parser{}
}

func (p parser) ParseFile(filename string) (f Database, _ error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return f, err
	}

	var report awsrds.Report
	err = json.Unmarshal(data, &report)
	if err != nil {
		return f, err
	}

	f = Database{
		AccountId:     report.Database.AccountId,
		Region:        report.Database.Region,
		ARN:           report.Database.ARN,
		Name:          report.Database.Name,
		Engine:        report.Database.Engine.Name,
		EngineVersion: report.Database.Engine.Version,
		Tags:          report.Database.Tags,
	}

	return f, nil
}

func (p parser) ParseFiles(glob string, pw progress.Writer) ([]Database, error) {
	files, err := filepath.Glob(glob)
	if err != nil {
		return nil, err
	}

	if files == nil {
		return nil, fmt.Errorf("no files could be found for glob %s", glob)
	}

	tracker := progress.Tracker{
		Message: "Parsing aws-rds-endoflife files",
		Total:   int64(len(files)),
	}
	pw.AppendTracker(&tracker)

	go pw.Render()

	var wg sync.WaitGroup
	databases := make([]Database, len(files))

	for i, f := range files {
		wg.Add(1)

		go func(i int, filename string) {
			defer wg.Done()
			defer tracker.Increment(1)

			f, err := p.ParseFile(filename)
			if err != nil {
				log.Printf("Failed to parse %s: %v", filename, err)
				return
			}
			databases[i] = f
		}(i, f)
	}

	wg.Wait()
	tracker.UpdateMessage(fmt.Sprintf("Parsed %d aws-rds-endoflife files", tracker.Total))
	tracker.MarkAsDone()

	return databases, nil
}
