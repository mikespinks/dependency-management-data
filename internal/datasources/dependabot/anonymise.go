package dependabot

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/dependabot/db"
)

func (*Dependabot) AnonymiseData(ctx context.Context, sqlDB *sql.DB, orgs []string) (err error) {
	depDB := db.New(sqlDB)
	err = depDB.AnonymiseOrgAndRepo(ctx)
	if err != nil {
		return
	}

	for _, org := range orgs {
		err = depDB.AnonymisePackageNameByOrg(ctx, org)
		if err != nil {
			return
		}

		err = depDB.AnonymisePackageFilePathByOrg(ctx, org)
		if err != nil {
			return
		}
	}

	return
}
