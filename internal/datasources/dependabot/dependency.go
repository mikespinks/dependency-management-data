package dependabot

import (
	"dmd.tanna.dev/internal/datasources/dependabot/db"
	"dmd.tanna.dev/internal/domain"
)

type Dependency struct {
	domain.Dependency
}

func newDependencyFromDB(dbRow db.Dependabot) Dependency {
	dep := Dependency{
		Dependency: domain.Dependency{
			Organisation:    dbRow.Organisation,
			Repo:            dbRow.Repo,
			PackageName:     dbRow.PackageName,
			Version:         dbRow.Version,
			PackageManager:  dbRow.PackageManager,
			PackageFilePath: dbRow.PackageFilePath,
		},
	}

	if dbRow.CurrentVersion.Valid {
		dep.CurrentVersion = &dbRow.CurrentVersion.String
	}

	return dep
}
