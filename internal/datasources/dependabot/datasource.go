package dependabot

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/dependabot/db"
)

type Dependabot struct{}

func (*Dependabot) Name() string {
	return "Dependabot"
}

func (*Dependabot) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
