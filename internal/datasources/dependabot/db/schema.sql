-- Deprecated: The `dependabot` table is no longer in use as of DMD v0.38.0, as
-- Dependabot data is now consumed via a Software Bill of Materials (SBOM), which uses the `sbom` table
CREATE TABLE IF NOT EXISTS dependabot (
  platform TEXT NOT NULL,
  organisation TEXT NOT NULL,
  repo TEXT NOT NULL,

  package_name TEXT NOT NULL,
  version TEXT NOT NULL,
  current_version TEXT,

  package_manager TEXT NOT NULL,
  package_file_path TEXT NOT NULL,

  UNIQUE (platform, organisation, repo, package_file_path, package_name, package_manager) ON CONFLICT REPLACE
);
