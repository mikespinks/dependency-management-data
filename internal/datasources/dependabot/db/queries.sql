-- name: InsertPackage :exec
INSERT INTO dependabot (
  platform,
  organisation,
  repo,

  package_name,
  version,
  current_version,

  package_manager,
  package_file_path
  ) VALUES (
  ?,
  ?,
  ?,

  ?,
  ?,
  ?,

  ?,
  ?
);

-- name: RetrieveAll :many
select * from dependabot;

-- name: AnonymiseOrgAndRepo :exec
update dependabot
set
  organisation  = 'ANON-' || hex(sha3(organisation)),
  repo          = 'ANON-' || hex(sha3(repo))
where
    organisation
    NOT LIKE 'ANON-%'
  OR
    repo
    NOT LIKE 'ANON-%'
;

-- name: AnonymisePackageNameByOrg :exec
update dependabot
set
  package_name  = 'ANON-' || hex(sha3(package_name))
where
    package_name like '%' || ? || '%'
;

-- name: AnonymisePackageFilePathByOrg :exec
update dependabot
set
  package_file_path  = 'ANON-' || hex(sha3(package_file_path))
where
    package_file_path like '%' || ? || '%'
;
