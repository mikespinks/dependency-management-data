package dependabot

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sync"

	"dmd.tanna.dev/internal/datasources/sbom/spdx"
	"dmd.tanna.dev/internal/domain"
	"github.com/jedib0t/go-pretty/v6/progress"
)

type parser struct{}

func NewParser() parser {
	return parser{}
}

type dependabotGraphData struct {
	Repo         string          `json:"repo"`
	Organisation string          `json:"organisation"`
	SBOM         json.RawMessage `json:"sbom"`
}

func (parser) ParseFile(filename string) ([]domain.SBOMDependency, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var depData dependabotGraphData
	err = json.Unmarshal(data, &depData)
	if err != nil {
		return nil, err
	}

	var format spdx.SPDXv2_3Format
	if !format.Matches(depData.SBOM) {
		return nil, fmt.Errorf("failed to parse file as an SPDX SBOM. Confirm that you're using >= v0.2.0 of dependabot-graph. Otherwise, raise an issue: %v", err)
	}

	deps := format.Parse(depData.SBOM, "github", depData.Organisation, depData.Repo)
	return deps, nil
}

func (p parser) ParseFiles(glob string, pw progress.Writer) ([]domain.SBOMDependency, error) {
	files, err := filepath.Glob(glob)
	if err != nil {
		return nil, err
	}

	if files == nil {
		return nil, fmt.Errorf("no files could be found for glob %s", glob)
	}

	tracker := progress.Tracker{
		Message: "Parsing Dependabot files",
		Total:   int64(len(files)),
	}
	pw.AppendTracker(&tracker)

	go pw.Render()

	var wg sync.WaitGroup
	allDeps := make([][]domain.SBOMDependency, len(files))

	for i, f := range files {
		wg.Add(1)

		go func(i int, f string) {
			defer wg.Done()
			defer tracker.Increment(1)

			deps, err := p.ParseFile(f)
			if err != nil {
				log.Printf("Failed to parse %s: %v", f, err)
				return
			}
			allDeps[i] = deps
		}(i, f)
	}

	wg.Wait()
	tracker.UpdateMessage(fmt.Sprintf("Parsed %d Dependabot files", tracker.Total))
	tracker.MarkAsDone()

	return flatten(allDeps), nil
}

func flatten(s [][]domain.SBOMDependency) []domain.SBOMDependency {
	var out []domain.SBOMDependency
	for _, v := range s {
		out = append(out, v...)
	}
	return out
}
