package spdx

import (
	"net/url"
	"strings"

	"github.com/package-url/packageurl-go"
)

// purlToPackageName converts a purl to a package name.
// Adapted from https://github.com/package-url/packageurl-go/blob/358f10025d4a923f7f9babc766f0968720e82240/packageurl.go#L176-L189, with better support for Maven coordinates
func purlToPackageName(p packageurl.PackageURL) string {
	var purl string

	// Add namespaces if provided
	if p.Namespace != "" {
		var ns []string
		for _, item := range strings.Split(p.Namespace, "/") {
			ns = append(ns, url.QueryEscape(item))
		}
		if p.Type == "maven" {
			purl = purl + strings.Join(ns, ":") + ":"
		} else {
			purl = purl + strings.Join(ns, "/") + "/"
		}
	}
	// The name is always required and must be a percent-encoded string
	// Use url.QueryEscape instead of PathEscape, as it handles @ signs
	purl = purl + url.QueryEscape(p.Name)

	return purl
}
