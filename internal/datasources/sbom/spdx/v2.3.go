package spdx

import (
	"bytes"
	"encoding/json"
	"strings"

	"dmd.tanna.dev/internal/domain"
	"github.com/package-url/packageurl-go"
	spdxJSON "github.com/spdx/tools-golang/json"
	"github.com/spdx/tools-golang/spdx/v2/v2_3"
)

type SPDXv2_3Format struct{}

type spdxv2_3Body struct {
	SPDXIdentifier string `json:"SPDXID"`
	SPDXVersion    string `json:"spdxVersion"`
}

func (s *SPDXv2_3Format) Matches(body []byte) bool {
	var b spdxv2_3Body
	err := json.Unmarshal(body, &b)
	if err != nil {
		return false
	}

	if b.SPDXIdentifier != "SPDXRef-DOCUMENT" || b.SPDXVersion != "SPDX-2.3" {
		return false
	}

	var doc v2_3.Document
	err = spdxJSON.ReadInto(bytes.NewReader(body), &doc)
	if err != nil {
		return false
	}
	return true
}

func (s *SPDXv2_3Format) Name() string {
	return "SPDX-2.3"
}

func (s *SPDXv2_3Format) Parse(body []byte, platform string, org string, repo string) []domain.SBOMDependency {
	var doc v2_3.Document
	err := spdxJSON.ReadInto(bytes.NewReader(body), &doc)
	if err != nil {
		return nil
	}

	return s.parseInternal(&doc, platform, org, repo)
}

func (s *SPDXv2_3Format) parseInternal(doc *v2_3.Document, platform string, org string, repo string) []domain.SBOMDependency {
	var deps []domain.SBOMDependency
	for i, v := range doc.Packages {
		// skip the first element, as it's the project itself
		if i == 0 {
			continue
		}

		var packageName, packageType string

		for _, ref := range v.PackageExternalReferences {
			if ref.Category != "PACKAGE-MANAGER" {
				continue
			}

			purl, err := packageurl.FromString(ref.Locator)
			if err != nil {
				continue
			}

			packageName = purlToPackageName(purl)
			packageType = purl.Type
		}

		if packageName == "" || packageType == "" {
			// TODO log an error here to note that we can't determine the packageName or packageManager
			continue
		}

		d := domain.SBOMDependency{
			Platform:       platform,
			Organisation:   org,
			Repo:           repo,
			PackageName:    strings.ReplaceAll(packageName, "%40", "@"),
			Version:        toNilIfEmpty(v.PackageVersion),
			CurrentVersion: currentVersion(v.PackageVersion),
			PackageType:    packageType,
		}

		deps = append(deps, d)
	}
	return deps
}
