package cyclonedx

import (
	"regexp"
)

var validVersionNumber = regexp.MustCompile("^[a-z0-9]")

func toNilIfEmpty(v string) *string {
	if v == "" {
		return nil
	}
	return &v
}

func currentVersion(v string) *string {
	if len(v) == 0 {
		return nil
	}

	if !validVersionNumber.MatchString(v) {
		return nil
	}

	return ptr(v)
}

func ptr[T any](v T) *T {
	return &v
}
