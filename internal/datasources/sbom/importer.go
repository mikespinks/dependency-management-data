package sbom

import (
	"context"
	"database/sql"
	"fmt"

	"dmd.tanna.dev/internal/datasources/sbom/db"
	"dmd.tanna.dev/internal/domain"
	"github.com/jedib0t/go-pretty/v6/progress"
)

type importer struct{}

func NewImporter() importer {
	return importer{}
}

func (importer) ImportDependencies(ctx context.Context, deps []domain.SBOMDependency, sqlDB *sql.DB, pw progress.Writer) error {
	d := db.New(sqlDB)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	tracker := progress.Tracker{
		Message: fmt.Sprintf("Importing %d SBOM dependencies", len(deps)),
		Total:   int64(len(deps)),
	}
	pw.AppendTracker(&tracker)

	go pw.Render()

	for _, dep := range deps {
		arg := db.InsertPackageParams{
			Platform:       dep.Platform,
			Organisation:   dep.Organisation,
			Repo:           dep.Repo,
			PackageName:    dep.PackageName,
			Version:        asSqlNullString(dep.Version),
			CurrentVersion: asSqlNullString(dep.CurrentVersion),
			PackageType:    dep.PackageType,
		}

		err := d.WithTx(tx).InsertPackage(ctx, arg)
		if err != nil {
			tracker.MarkAsErrored()
			return err
		}

		tracker.Increment(1)
	}

	tracker.MarkAsDone()
	tracker.UpdateMessage(fmt.Sprintf("Imported %d SBOM dependencies", tracker.Total))
	return tx.Commit()
}

func asSqlNullString(s *string) sql.NullString {
	if s == nil {
		return sql.NullString{}
	}

	return sql.NullString{
		Valid:  true,
		String: *s,
	}
}
