package sbom

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/sbom/db"
)

type SBOMs struct{}

func (*SBOMs) Name() string {
	return "SBOMs"
}

func (*SBOMs) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}

func (d *SBOMs) AnonymiseData(ctx context.Context, sqlDB *sql.DB, orgs []string) (err error) {
	queries := db.New(sqlDB)

	err = queries.AnonymiseOrgAndRepo(ctx)
	if err != nil {
		return
	}

	for _, org := range orgs {
		err = queries.AnonymisePackageNameByOrg(ctx, org)
		if err != nil {
			return
		}
	}

	return
}
