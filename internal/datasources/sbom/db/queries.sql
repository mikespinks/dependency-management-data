-- name: InsertPackage :exec
INSERT INTO sboms (
  platform,
  organisation,
  repo,
  package_name,
  version,
  current_version,
  package_type
) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);

-- name: AnonymiseOrgAndRepo :exec
update sboms
set
  organisation  = 'ANON-' || hex(sha3(organisation)),
  repo          = 'ANON-' || hex(sha3(repo))
where
  organisation
    NOT LIKE 'ANON-%'
  OR
  repo
    NOT LIKE 'ANON-%'
;

-- name: AnonymisePackageNameByOrg :exec
update sboms
set
  package_name  = 'ANON-' || hex(sha3(package_name))
where
    package_name like '%' || ? || '%'
;

-- name: RetrieveDistinctPackages :many
select
  distinct
  package_name,
  version,
  current_version,
  package_type
from
  sboms
where
  version is not null
  or
  current_version is not null
;

-- name: RetrieveReposAndPackagesForMissingData :many
select
  platform,
  organisation,
  repo,
  package_name,
  version,
  current_version,
  package_type
from
  sboms
where
  version is not null
  or
  current_version is not null
;
