package awslambda

import (
	"context"
	"database/sql"
	"log/slog"
	"time"

	"dmd.tanna.dev/internal/datasources/awslambda/db"
	"github.com/jedib0t/go-pretty/v6/progress"
	"gitlab.com/tanna.dev/endoflife-checker/awslambda/runtimes"
)

func GenerateEndOfLife(ctx context.Context, logger *slog.Logger, sqlDB *sql.DB, pw progress.Writer) error {
	d := db.New(sqlDB)

	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	data := runtimes.DeprecationData()

	for k, di := range data {
		params := db.InsertLambdaFunctionRuntimeParams{
			Runtime: string(k),
		}

		if di.Deprecation != nil {
			params.Deprecation = sql.NullString{
				String: di.Deprecation.Format(time.RFC3339),
				Valid:  true,
			}
		}

		if di.EndOfLife != nil {
			params.EndOfLife = sql.NullString{
				String: di.EndOfLife.Format(time.RFC3339),
				Valid:  true,
			}
		}

		err := d.WithTx(tx).InsertLambdaFunctionRuntime(ctx, params)
		if err != nil {
			return err
		}

	}

	return tx.Commit()
}
