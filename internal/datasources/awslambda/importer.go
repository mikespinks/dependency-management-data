package awslambda

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"

	"dmd.tanna.dev/internal/datasources/awslambda/db"
	"github.com/jedib0t/go-pretty/v6/progress"
)

type importer struct{}

func NewImporter() importer {
	return importer{}
}

func (importer) ImportFunctions(ctx context.Context, functions []LambdaFunction, sqlDB *sql.DB, pw progress.Writer) error {
	d := db.New(sqlDB)
	tx, err := sqlDB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	tracker := progress.Tracker{
		Message: fmt.Sprintf("Importing %d aws-lambda-endoflife functions", len(functions)),
		Total:   int64(len(functions)),
	}
	pw.AppendTracker(&tracker)

	go pw.Render()

	for _, f := range functions {
		arg := db.InsertLambdaFunctionParams{
			AccountID: f.AccountId,
			Region:    f.Region,
			Arn:       f.ARN,
			Name:      f.Name,
			Runtime:   f.Runtime,
		}

		data, err := json.Marshal(f.Tags)
		if err != nil {
			tracker.MarkAsErrored()
			return err
		}

		arg.Tags = string(data)

		if f.LastModified != nil {
			arg.LastModified = sql.NullString{
				String: *f.LastModified,
				Valid:  true,
			}
		}

		err = d.WithTx(tx).InsertLambdaFunction(ctx, arg)
		if err != nil {
			tracker.MarkAsErrored()
			return err
		}

		tracker.Increment(1)
	}

	tracker.MarkAsDone()
	tracker.UpdateMessage(fmt.Sprintf("Imported %d aws-lambda-endoflife functions", tracker.Total))

	return tx.Commit()
}
