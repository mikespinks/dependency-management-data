CREATE TABLE IF NOT EXISTS aws_lambda_functions (
  account_id TEXT NOT NULL,
  region TEXT NOT NULL,
  arn TEXT NOT NULL,
  name TEXT NOT NULL,
  runtime TEXT NOT NULL,
  last_modified TEXT,

  -- tags is a JSON object
  tags TEXT NOT NULL,

  UNIQUE (account_id, region, arn) ON CONFLICT REPLACE
);

CREATE TABLE IF NOT EXISTS aws_lambda_function_runtimes (
  runtime TEXT NOT NULL,
	-- deprecation marks the "Phase 1" deprecations as noted in https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html:
	-- > Phase 1 - Lambda no longer applies security patches or other updates to
	-- > the runtime. You can no longer **create** functions that use the runtime, but
	-- > you can continue to update existing functions. This includes updating the
	-- > runtime version, and rolling back to the previous runtime version. Note
	-- > that functions that use a deprecated runtime are no longer eligible for
	-- > technical support
	deprecation TEXT,
  -- end_of_life marks the "Phase 2" deprecations as noted in https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html
	-- > Phase 2 - you can no longer **create or update** functions that use the
	-- > runtime. To update a function, you need to migrate it to a supported
	-- > runtime version. After you migrate the function to a supported runtime
	-- > version, you cannot rollback the function to the previous runtime. Phase 2
	-- > starts at least 30 days after the start of Phase 1
  end_of_life TEXT,

  UNIQUE (runtime) ON CONFLICT REPLACE
);
