package awslambda

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/awslambda/db"
)

func RetrieveAll(ctx context.Context, sqlDB *sql.DB) ([]LambdaFunction, error) {
	queries := db.New(sqlDB)

	rows, err := queries.RetrieveAll(ctx)
	if err != nil {

		return nil, err
	}

	functions := make([]LambdaFunction, len(rows))

	for i, r := range rows {
		functions[i] = newLambdaFunctionFromDB(r)
	}

	return functions, nil
}
