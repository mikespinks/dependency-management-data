package awslambda

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/awslambda/db"
)

func (*AWSLambda) AnonymiseData(ctx context.Context, sqlDB *sql.DB, orgs []string) (err error) {
	lambdaDB := db.New(sqlDB)
	err = lambdaDB.AnonymiseLambdaFunctions(ctx)
	return err
}
