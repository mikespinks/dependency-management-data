package awslambda

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/awslambda/db"
)

type AWSLambda struct{}

func (*AWSLambda) Name() string {
	return "AWS Lambda"
}

func (*AWSLambda) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
