package queries

type Count struct {
	Name  string
	Count int64
}

type Repo struct {
	Platform     string
	Organisation string
	Repo         string
}

type RepoWithOwner struct {
	Platform     string
	Organisation string
	Repo         string
	Owner        *string
}
