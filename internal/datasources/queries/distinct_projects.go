package queries

type DistinctProjectParams struct {
	Platform     *string
	Organisation *string
	Repo         *string
}

type DistinctProject struct {
	Platform     string
	Organisation string
	Repo         string
}
