package awselasticache

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/awselasticache/db"
)

func (*AWSElasticache) AnonymiseData(ctx context.Context, sqlDB *sql.DB, orgs []string) (err error) {
	rdsDB := db.New(sqlDB)
	err = rdsDB.AnonymiseElasticacheDatastores(ctx)
	return err
}
