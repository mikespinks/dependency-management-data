package awselasticache

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/awselasticache/db"
)

func RetrieveAll(ctx context.Context, sqlDB *sql.DB) ([]Datastore, error) {
	queries := db.New(sqlDB)

	rows, err := queries.RetrieveAll(ctx)
	if err != nil {

		return nil, err
	}

	datastores := make([]Datastore, len(rows))

	for i, r := range rows {
		datastores[i] = newDatastoreFromDB(r)
	}

	return datastores, nil
}
