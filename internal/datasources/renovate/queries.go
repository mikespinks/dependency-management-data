package renovate

import (
	"context"
	"database/sql"
	"sort"
	"strings"

	"dmd.tanna.dev/internal/datasources/queries"
	"dmd.tanna.dev/internal/datasources/renovate/db"
)

func RetrieveAll(ctx context.Context, sqlDB *sql.DB) ([]Dependency, error) {
	queries := db.New(sqlDB)

	rows, err := queries.RetrieveAll(ctx)
	if err != nil {

		return nil, err
	}

	deps := make([]Dependency, len(rows))

	for i, r := range rows {
		deps[i] = newDependencyFromDB(r)
	}

	return deps, nil
}

func (*Renovate) QueryMostPopularPackageManagers(ctx context.Context, sqlDB *sql.DB) ([]queries.MostPopularPackageManagersRow, error) {
	q := db.New(sqlDB)

	rows, err := q.QueryMostPopularPackageManagers(ctx)
	if err != nil {
		return nil, err
	}

	results := make([]queries.MostPopularPackageManagersRow, len(rows))
	for i, row := range rows {
		results[i] = queries.MostPopularPackageManagersRow{
			PackageManager: row.PackageManager,
			Count:          row.Count,
		}
	}
	return results, nil
}

func (*Renovate) QueryMostPopularDockerImages(ctx context.Context, sqlDB *sql.DB) (result queries.MostPopularDockerImagesResult, err error) {
	q := db.New(sqlDB)

	rows, err := q.QueryMostPopularDockerImages(ctx)
	if err != nil {
		return
	}

	namespaces := make(map[string]int64)
	images := make(map[string]int64)

	for _, row := range rows {
		images[row.PackageName] = row.Count

		var namespace string
		parts := strings.Split(row.PackageName, "/")
		if len(parts) == 1 {
			namespace = "_"
		} else if len(parts) == 2 {
			namespace = parts[0]
		} else if strings.Contains(parts[0], ".") {
			namespace = strings.Join(parts[:len(parts)-1], "/")
		} else {
			// this ideally shouldn't get hit, but if it does, return the full package name
			namespace = row.PackageName
		}

		current := namespaces[namespace]
		namespaces[namespace] = current + row.Count
	}

	result.Namespaces = descendingSort(namespaces)
	result.Images = descendingSort(images)

	return
}

func (*Renovate) QueryGolangCILint(ctx context.Context, sqlDB *sql.DB) (result queries.GolangCILintResult, err error) {
	q := db.New(sqlDB)

	directRows, err := q.QueryGolangCILintDirect(ctx)
	if err != nil {
		return
	}

	indirectRows, err := q.QueryGolangCILintIndirect(ctx)
	if err != nil {
		return
	}

	result.Direct = make([]queries.RepoWithOwner, len(directRows))

	for i, row := range directRows {
		result.Direct[i] = queries.RepoWithOwner{
			Platform:     row.Platform,
			Organisation: row.Organisation,
			Repo:         row.Repo,
			Owner:        sqlNullStringToStringPointer(row.Owner),
		}
	}

	result.Indirect = make([]queries.RepoWithOwner, len(indirectRows))

	for i, row := range indirectRows {
		result.Indirect[i] = queries.RepoWithOwner{
			Platform:     row.Platform,
			Organisation: row.Organisation,
			Repo:         row.Repo,
			Owner:        sqlNullStringToStringPointer(row.Owner),
		}
	}

	return
}

func (*Renovate) QueryDistinctProjects(ctx context.Context, sqlDB *sql.DB, queryParams queries.DistinctProjectParams) ([]queries.DistinctProject, error) {
	q := db.New(sqlDB)

	params := db.QueryDistinctProjectsParams{
		Platform:     "%",
		Organisation: "%",
		Repo:         "%",
	}

	if queryParams.Platform != nil && *queryParams.Platform != "*" {
		params.Platform = *queryParams.Platform
	}
	if queryParams.Organisation != nil && *queryParams.Organisation != "*" {
		params.Organisation = *queryParams.Organisation
	}
	if queryParams.Repo != nil && *queryParams.Repo != "*" {
		params.Repo = *queryParams.Repo
	}

	// allow wildcarding
	params.Platform = strings.ReplaceAll(params.Platform, "*", "%")
	params.Organisation = strings.ReplaceAll(params.Organisation, "*", "%")
	params.Repo = strings.ReplaceAll(params.Repo, "*", "%")

	rows, err := q.QueryDistinctProjects(ctx, params)
	if err != nil {
		return nil, err
	}

	var projects []queries.DistinctProject
	for _, row := range rows {
		projects = append(projects, queries.DistinctProject{
			Platform:     row.Platform,
			Organisation: row.Organisation,
			Repo:         row.Repo,
		})
	}

	return projects, nil
}

func descendingSort(m map[string]int64) []queries.Count {
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}

	sort.SliceStable(keys, func(i, j int) bool {
		return m[keys[i]] > m[keys[j]]
	})

	result := make([]queries.Count, len(keys))
	for i, key := range keys {
		result[i] = queries.Count{
			Name:  key,
			Count: m[key],
		}
	}

	return result
}

func sqlNullStringToStringPointer(s sql.NullString) *string {
	if !s.Valid {
		return nil
	}

	return &s.String
}
