-- name: InsertPackage :exec
INSERT INTO renovate (
  platform,
  organisation,
  repo,

  package_name,
  version,
  current_version,

  package_manager,
  package_file_path,

  datasource,
  dep_types
  ) VALUES (
  ?,
  ?,
  ?,

  ?,
  ?,
  ?,

  ?,
  ?,

  ?,
  ?
);

-- name: RetrieveAll :many
select * from renovate;

-- name: RetrieveDistinctPackages :many
select
  distinct
  package_name,
  version,
  current_version,
  package_manager,
  datasource
from
  renovate;

-- name: RetrieveReposAndPackagesForMissingData :many
select
  platform,
  organisation,
  repo,
  package_name,
  version,
  current_version,
  package_manager,
  package_file_path,
  datasource,
  dep_types
from
  renovate
where dep_types not like '%"missing-data"%'
;

-- name: AnonymiseOrgAndRepo :exec
update renovate
set
  organisation  = 'ANON-' || hex(sha3(organisation)),
  repo          = 'ANON-' || hex(sha3(repo))
where
  organisation
    NOT LIKE 'ANON-%'
  OR
  repo
    NOT LIKE 'ANON-%'
;

-- name: AnonymisePackageNameByOrg :exec
update renovate
set
  package_name  = 'ANON-' || hex(sha3(package_name))
where
    package_name like '%' || ? || '%'
;

-- name: AnonymisePackageFilePathByOrg :exec
update renovate
set
  package_file_path  = 'ANON-' || hex(sha3(package_file_path))
where
    package_file_path like '%' || ? || '%'
;

-- name: AnonymiseVersionByOrg :exec
update renovate
set
  version  = 'ANON-' || hex(sha3(version))
where
    version like '%' || ? || '%'
;

-- name: QueryMostPopularPackageManagers :many
SELECT
package_manager,
count(*)
from renovate
group by
package_manager
order by count(*) desc;

-- name: QueryMostPopularDockerImages :many
select
package_name, count(package_name)
from renovate
where datasource = 'docker'
and package_name != ''
group by package_name;

-- preferably with a `json_each` but due to https://github.com/sqlc-dev/sqlc/issues/1830 we need to handle it like this
-- name: QueryGolangCILintDirect :many
select
distinct
renovate.platform,
renovate.organisation,
renovate.repo,
owner
from
renovate,
json_each(renovate.dep_types) as dep_type
left join owners on
renovate.platform = owners.platform and
renovate.organisation = owners.organisation and
renovate.repo = owners.repo
where
package_name = 'github.com/golangci/golangci-lint'
and package_manager = 'gomod'
and dep_type.value = 'require';

-- preferably with a `json_each` but due to https://github.com/sqlc-dev/sqlc/issues/1830 we need to handle it like this
-- name: QueryGolangCILintIndirect :many
select
distinct
renovate.platform,
renovate.organisation,
renovate.repo,
owner
from
renovate,
json_each(renovate.dep_types) as dep_type
left join owners on
renovate.platform = owners.platform and
renovate.organisation = owners.organisation and
renovate.repo = owners.repo
where
package_name = 'github.com/golangci/golangci-lint'
and package_manager = 'gomod'
and dep_type.value = 'indirect';

-- name: QueryDistinctProjects :many
select
distinct
platform,
organisation,
repo
from
renovate
where
platform like ?
and organisation like ?
and repo like ?
;

-- name: InsertPackageUpdate :exec
INSERT INTO renovate_updates (
  platform,
  organisation,
  repo,

  package_name,
  version,
  current_version,

  package_manager,
  package_file_path,

  datasource,

  new_version,
  update_type
  ) VALUES (
  ?,
  ?,
  ?,

  ?,
  ?,
  ?,

  ?,
  ?,

  ?,

  ?,
  ?
);

-- name: AnonymiseUpdatesOrgAndRepo :exec
update renovate_updates
set
  organisation  = 'ANON-' || hex(sha3(organisation)),
  repo          = 'ANON-' || hex(sha3(repo))
where
  organisation
    NOT LIKE 'ANON-%'
  OR
  repo
    NOT LIKE 'ANON-%'
;

-- name: AnonymiseUpdatesPackageNameByOrg :exec
update renovate_updates
set
  package_name  = 'ANON-' || hex(sha3(package_name))
where
    package_name like '%' || ? || '%'
;

-- name: AnonymiseUpdatesPackageFilePathByOrg :exec
update renovate_updates
set
  package_file_path  = 'ANON-' || hex(sha3(package_file_path))
where
    package_file_path like '%' || ? || '%'
;

-- name: AnonymiseUpdatesVersionByOrg :exec
update renovate_updates
set
  version  = 'ANON-' || hex(sha3(version))
where
    version like '%' || ? || '%'
;
