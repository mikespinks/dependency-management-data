CREATE TABLE IF NOT EXISTS renovate (
  platform TEXT NOT NULL,
  organisation TEXT NOT NULL,
  repo TEXT NOT NULL,

  package_name TEXT NOT NULL,
  -- this could be a version constraint, such as `<=1.3.4,>=1.3.0`
  version TEXT NOT NULL,
  -- current_version is derived from:
  -- - the `locked_version` or `fixed_version`, if there is a lockfile or some pinning set
  -- - the `current_version` if present, and can be recalculated each time the data is generated
  -- this will give you an exact version number, such as `1.3.4`
  current_version TEXT,

  package_manager TEXT NOT NULL,
  package_file_path TEXT NOT NULL,

  datasource TEXT NOT NULL,
  -- dep_types is a JSON array
  dep_types TEXT NOT NULL,

  UNIQUE (platform, organisation, repo, package_file_path, package_name, package_manager, dep_types) ON CONFLICT REPLACE
);

CREATE TABLE IF NOT EXISTS renovate_updates (
  platform TEXT NOT NULL,
  organisation TEXT NOT NULL,
  repo TEXT NOT NULL,

  package_name TEXT NOT NULL,
  -- this could be a version constraint, such as `<=1.3.4,>=1.3.0`
  version TEXT NOT NULL,
  -- current_version is derived from:
  -- - the `locked_version` or `fixed_version`, if there is a lockfile or some pinning set
  -- - the `current_version` if present, and can be recalculated each time the data is generated
  -- this will give you an exact version number, such as `1.3.4`
  current_version TEXT,

  package_manager TEXT NOT NULL,
  package_file_path TEXT NOT NULL,

  datasource TEXT NOT NULL,

  -- custom for package updates
  new_version TEXT NOT NULL,
  update_type TEXT NOT NULL,

  UNIQUE (platform, organisation, repo, package_file_path, package_name, package_manager, update_type) ON CONFLICT REPLACE
);
