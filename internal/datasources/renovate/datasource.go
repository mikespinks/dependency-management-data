package renovate

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/renovate/db"
)

type Renovate struct{}

func (*Renovate) Name() string {
	return "Renovate"
}

func (*Renovate) CreateTables(ctx context.Context, sqlDB *sql.DB) error {
	_, err := sqlDB.ExecContext(ctx, db.CreateTablesQuery)
	return err
}
