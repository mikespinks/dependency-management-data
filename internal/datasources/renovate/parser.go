package renovate

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"dmd.tanna.dev/internal/domain"
	"github.com/jedib0t/go-pretty/v6/progress"
)

type parser struct{}

func NewParser() parser {
	return parser{}
}

type packageData map[string][]struct {
	Deps []struct {
		DepName     string `json:"depName"`
		PackageName string `json:"packageName"`
		// PackageManager comes from the map key
		Datasource     string   `json:"datasource"`
		DepType        string   `json:"depType"`
		DepTypes       []string `json:"depTypes"`
		CurrentValue   string   `json:"currentValue"`
		LockedVersion  string   `json:"lockedVersion"`
		FixedVersion   string   `json:"fixedVersion"`
		CurrentVersion string   `json:"currentVersion"`
		Updates        []struct {
			NewVersion string `json:"newVersion"`
			NewValue   string `json:"newValue"`
			UpdateType string `json:"updateType"`
		} `json:"updates"`
	} `json:"deps"`
	PackageFile string `json:"packageFile"`
}

type renovateDebugLogEntry struct {
	LogContext string      `json:"logContext"`
	Repository string      `json:"repository"`
	Config     packageData `json:"config"`
}

type renovateGraphData struct {
	Organisation string      `json:"organisation"`
	Repo         string      `json:"repo"`
	PackageData  packageData `json:"packageData"`
	Metadata     struct {
		Renovate struct {
			Platform string `json:"platform"`
		} `json:"renovate"`
	} `json:"metadata"`
}

func (p parser) ParseFile(filename string) ([]Dependency, []DependencyUpdate, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return nil, nil, err
	}

	var logEntry renovateDebugLogEntry
	err = json.Unmarshal(data, &logEntry)
	if err != nil {
		return nil, nil, err
	}

	if logEntry.LogContext != "" {
		org, repo := parseRepoWithOrg(logEntry.Repository)
		return p.parsePackageData("unknown", org, repo, logEntry.Config)
	}

	return p.parseRenovateGraphData(data)
}

func (p parser) parseRenovateGraphData(data []byte) ([]Dependency, []DependencyUpdate, error) {
	var rgData renovateGraphData
	err := json.Unmarshal(data, &rgData)
	if err != nil {
		return nil, nil, err
	}

	return p.parsePackageData(rgData.Metadata.Renovate.Platform, rgData.Organisation, rgData.Repo, rgData.PackageData)
}

func (parser) parsePackageData(platform string, organisation string, repo string, data packageData) ([]Dependency, []DependencyUpdate, error) {
	var deps []Dependency
	var depUpdates []DependencyUpdate
	for packageManager, v := range data {
		for _, container := range v {
			for _, dep := range container.Deps {
				dep := dep

				versions := strings.Split(dep.CurrentValue, "\n")
				for _, version := range versions {
					d := Dependency{
						Dependency: domain.Dependency{
							Platform:     platform,
							Organisation: organisation,
							Repo:         repo,

							// PackageName is being added below
							Version: version,
							// CurrentVersion is being added below

							PackageManager:  packageManager,
							PackageFilePath: container.PackageFile,
						},
						Datasource: dep.Datasource,

						// DepTypes is being added below
					}

					if dep.DepName != "" && !strings.Contains(dep.DepName, "\n") {
						d.PackageName = dep.DepName
					} else if dep.PackageName != "" && !strings.Contains(dep.PackageName, "\n") {
						d.PackageName = dep.PackageName
					}

					if dep.LockedVersion != "" && dep.FixedVersion == "" {
						d.CurrentVersion = &dep.LockedVersion
					} else if dep.LockedVersion == "" && dep.FixedVersion != "" {
						d.CurrentVersion = &dep.FixedVersion
					} else if dep.LockedVersion != "" && dep.FixedVersion != "" {
						d.CurrentVersion = &dep.LockedVersion
						if dep.LockedVersion != dep.FixedVersion {
							// TODO warn that they're not the same, but we're only using one of them
						}
					} else if dep.CurrentVersion != "" {
						d.CurrentVersion = &dep.CurrentVersion
					}

					if dep.DepTypes != nil {
						d.DepTypes = dep.DepTypes
					} else if dep.DepType != "" {
						d.DepTypes = []string{dep.DepType}
					}

					deps = append(deps, d)

					for _, update := range dep.Updates {
						depUpdate := DependencyUpdate{
							DependencyUpdate: domain.DependencyUpdate{
								Dependency: d.Dependency,

								NewVersion: update.NewVersion,
								UpdateType: update.UpdateType,
							},
							Datasource: d.Datasource,
						}

						if depUpdate.NewVersion == "" {
							depUpdate.NewVersion = update.NewValue
						}

						depUpdates = append(depUpdates, depUpdate)
					}
				}
			}
		}
	}

	return deps, depUpdates, nil
}

func (p parser) ParseFiles(glob string, pw progress.Writer) ([]Dependency, []DependencyUpdate, error) {
	files, err := filepath.Glob(glob)
	if err != nil {
		return nil, nil, err
	}

	if files == nil {
		return nil, nil, fmt.Errorf("no files could be found for glob %s", glob)
	}

	tracker := progress.Tracker{
		Message: "Parsing Renovate files",
		Total:   int64(len(files)),
	}
	pw.AppendTracker(&tracker)

	go pw.Render()

	var wg sync.WaitGroup
	allDeps := make([][]Dependency, len(files))
	allDepUpdates := make([][]DependencyUpdate, len(files))

	for i, f := range files {
		wg.Add(1)

		go func(i int, f string) {
			defer wg.Done()
			defer tracker.Increment(1)

			deps, depUpdates, err := p.ParseFile(f)
			if err != nil {
				log.Printf("Failed to parse %s: %v", f, err)
				return
			}
			allDeps[i] = deps
			allDepUpdates[i] = depUpdates
		}(i, f)
	}

	wg.Wait()
	tracker.UpdateMessage(fmt.Sprintf("Parsed %d Renovate files", tracker.Total))
	tracker.MarkAsDone()

	return flatten(allDeps), flatten(allDepUpdates), nil
}

func flatten[T any](s [][]T) []T {
	var out []T
	for _, v := range s {
		out = append(out, v...)
	}
	return out
}

func parseRepoWithOrg(s string) (org string, repo string) {
	return filepath.Dir(s), filepath.Base(s)
}
