# Adding a new parser for EndOfLife functionality

To add support for new products that [EndOfLife.date](https://endoflife.date/) supports, we can start by using the boilerplate generator in `cmd/gen-endoflifedate-parser` which simplifies some of the wiring, and makes it more straightforward to get up-and-running.

To run this:

```sh
# i.e.
go run cmd/gen-endoflifedate-parser/main.go  -datasource renovate -product spring-boot
# i.e.
go run cmd/gen-endoflifedate-parser/main.go  -datasource sbom -product symfony
```

**Note** that this will overwrite any existing files.

Once the boilerplate is created, you will need to amend the generated stub to provide a real implementation. It's recommended to refer to the existing files, for instance the Ruby parser, which handles dependency information from different datasources (`.ruby-version`, `Dockerfile`, and others).

It's worth having existing package data that presents the different sources of the package name, if possible, as it'll make it easier to determine if the parsing works.
