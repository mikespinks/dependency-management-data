---
title: Web Application
---
dependency-management-data comes with a web application (`dmd-web`) to make it easier to have a central place to go to view your database.

`dmd-web` embeds [Datasette](https://datasette.io) as an interactive SQLite browser, so you can query the data without needing to have any tools downloaded locally. The web-exposed nature of it also allows you to share common queries in a URL format, rather than passing around complex SQL statements!

`dmd-web` also has support for viewing the same [reports](/concepts/report/) that can be generated on the command-line through the browser.

The example data has an associated DMD web application which can be found at [dependency-management-data-example.fly.dev](https://dependency-management-data-example.fly.dev/).

User-provided configuration can be added to the web application to provide context around what the app is, information about when the data was last updated, and also allows for adding custom key-value data. Check out [`cmd/dmd-web/userconfig.go`](https://gitlab.com/tanna.dev/dependency-management-data/-/blob/main/cmd/dmd-web/userconfig.go) for more detail.
