---
title: Features
---
<style>
th {
  text-align: center;
  text-justify: inter-word;
}

/* only the second+ rows, via https://stackoverflow.com/a/15675231 */
td + td {
  text-align: center;
  text-justify: inter-word;
}
</style>

A directory of the features that dependency-management-data supports across different [datasources](/concepts/datasource/).

## Feature

<table>
<tr>
<th>
Feature
</th>
<th>
Renovate
</th>
<th>
Software Bill <br> of Materials <br> (SBOM)
</th>
<th>
AWS Infrastructure
</th>
</tr>
<tr>
</tr>

<tr>
<td>
    Advisories
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
    Database anonymisation
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>

<tr>
<td>
    End-of-Life lookups, via <a href="https://endoflife.date">EndOfLife.date</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    CVE Lookups, via <a href="https://osv.dev">osv.dev</a>
</td>
<td>
✅
</td>
<td>
</td>
<td>
</td>
</tr>

<tr>
<td>
    Licensing + CVE Lookups, via <a href="https://deps.dev">deps.dev</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
<td>
</td>
</tr>

</table>

## SBOM formats

- CycloneDX v1.4 (JSON, YAML)
- SPDX v2.3 (JSON, YAML)

## Reports

<table>
<tr>
<th>
Report
</th>
<th>
Renovate
</th>
<th>
Software Bill <br> of Materials <br> (SBOM)
</th>
</tr>
<tr>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_advisories">advisories</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_licenses">licenses</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_golangcilint">golangCILint</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_mostpopulardockerimages">mostPopularDockerImages</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
<a href="/commands/dmd_report_mostpopularpackagemanagers">mostPopularPackageManagers</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

</table>

## End-of-Life checking

Via [endoflife.date](https://endoflife.date)

<table>
<tr>
<th>
Product
</th>
<th>
Renovate
</th>
<th>
Software Bill <br> of Materials <br> (SBOM)
</th>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/go>Go</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/alpine>Alpine</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/nodejs>NodeJS</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/python>Python</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/redis>Redis</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/ruby>Ruby</a>
</td>
<td>
✅
</td>
<td>
</td>
</tr>

<tr>
<td>
    <a href=https://endoflife.date/rails>Ruby on Rails</a>
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

</table>

## CVE + license checking

Via [deps.dev](https://deps.dev)

<table>
<tr>
<th>
Ecosystem
</th>
<th>
Renovate
</th>
<th>
Software Bill <br> of Materials <br> (SBOM)
</th>
</tr>

<tr>
<td>
npm
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
    Go
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
    Maven
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
    PyPI
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
    NuGet
</td>
<td>
</td>
<td>
</td>
</tr>

<tr>
<td>
    Cargo
</td>
<td>
✅
</td>
<td>
</td>
</tr>

</table>

## Generating missing package data

Via [deps.dev](https://deps.dev)

<table>
<tr>
<th>
Ecosystem
</th>
<th>
Renovate
</th>
<th>
Software Bill <br> of Materials <br> (SBOM)
</th>
</tr>

<tr>
<td>
npm
</td>
<td>
</td>
<td>
</td>
</tr>

<tr>
<td>
    Go
</td>
<td>
</td>
<td>
</td>
</tr>

<tr>
<td>
    Maven
</td>
<td>
✅
</td>
<td>
✅
</td>
</tr>

<tr>
<td>
    PyPI
</td>
<td>
</td>
<td>
</td>
</tr>

<tr>
<td>
    NuGet
</td>
<td>
</td>
<td>
</td>
</tr>

<tr>
<td>
    Cargo
</td>
<td>
</td>
<td>
</td>
</tr>

</table>
