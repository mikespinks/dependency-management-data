---
title: Related
---
Dependency-management-data relies on various tools for producing the [datasources](/concepts/datasource/) that are used by the `dmd` CLI:

- [renovate-graph](https://gitlab.com/tanna.dev/renovate-graph) for using [Renovate](https://docs.renovatebot.com)'s excellent support for package ecosystems to extract dependency data
- [dependabot-graph](https://gitlab.com/tanna.dev/dependabot-graph) for using GitHub Advanced Security's Dependabot dependency graph functionality to extract dependency data
- [endoflife-checker](https://gitlab.com/tanna.dev/endoflife-checker) for various types of infrastructure lookups, such as AWS Lambda and RDS

Other related discussions about dependency-management-data:

- [Video: Jamie Tanna speaking at DevOpsDays London 2023 about dependency-management-data](https://www.youtube.com/watch?v=nTt-TVHGZZk)
- [Blog post: Quantifying your reliance on Open Source software](https://www.jvt.me/posts/2023/07/25/dmd-talk/) - an in-depth look at what dependency-management-data is, how it came to be, and some case studies for how it's been used in the past
- [Blog post: Utilising Renovate's `local` platform to make `renovate-graph` more efficient](https://www.jvt.me/posts/2023/10/13/renovate-graph-local/)
- [Blog post: Custom Advisories: the unsung hero of dependency-management-data](https://www.jvt.me/posts/2023/08/29/dmd-custom-advisories/)
- [Blog post: Using dependency-management-data with GitLab's Pipeline-specific CycloneDX SBOM exports](https://www.jvt.me/posts/2023/09/27/dmd-gitlab/)
