---
title: Example
asciinema: true
---
The below examples are based on [the example repo on GitLab.com](https://gitlab.com/tanna.dev/dependency-management-data-example/).

You can also check out the DMD web application which corresponds with this data at [dependency-management-data-example.fly.dev](https://dependency-management-data-example.fly.dev/).

It's recommended to have a look at the repo and have a play with the data, but an example of the different things you can do with the project can be found below.

## Initialise database and import datasources

{{< asciinema id="initAndImport" >}}

## Set ownership information

{{< asciinema id="setOwners" >}}

## Report for usages of golangci-lint which are tracked as a source dependency

{{< asciinema id="reportGolangCILint" >}}

## Report the most popular Docker namespaces and images

{{< asciinema id="mostPopularDockerImages" >}}

## Report the most popular package managers

{{< asciinema id="mostPopularPackageManagers" >}}

## Report package advisories

Includes custom advisories ("this package is deprecated", "our organisation doesn't want to use that library") as well as End-of-Life (EOL) checking

{{< asciinema id="generateAdvisoryAndList" >}}

Additionally, through the `report advisories` command (There's a lot to display here, so it disappears pretty quickly 😅):

{{< asciinema id="reportAdvisories" >}}

## Report license information for package dependencies

{{< asciinema id="reportLicenses" >}}
