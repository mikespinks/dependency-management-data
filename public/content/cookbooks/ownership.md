---
title: Using repository ownership information with dependency-management-data
---
One of the benefits of having dependency-management-data is that you can see across your organisation's usage of Open Source and internal dependencies.

If you were for instance looking at how your organisation is affected by [advisories](/concepts/advisory/), one key piece of information is "who can I talk to about this issue", either to work to resolve it, or discover if it's a false positive.

This is where dependency-management-data's understanding of ownership comes in, which allows setting a per-repo owner (coming soon: [multiple owners](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/112)).

## Syncing data

Depending on your data source, such as a service catalogue like Backstage, Service Now, or some other source of your ownership data, the means for you to _retrieve_ the data is up to you, but you can then take that data and ingest it into dependency-management-data.

### Via `dmd owners set`

One option we have is to use the `dmd` CLI to set the ownership information:

```sh
dmd --db dmd.db owners set 'Jamie Tanna'          --organisation jamietanna
dmd --db dmd.db owners set 'Jamie Tanna'          --organisation tanna.dev
dmd --db dmd.db owners set 'Jamie Tanna'          --organisation jamietanna
# notice that you can include a basic wildcard
dmd --db dmd.db owners set 'GDS Pay'              --organisation alphagov   --repo 'pay-*'
dmd --db dmd.db owners set 'GDS Digital Identity' --organisation alphagov   --repo 'di-*'
dmd --db dmd.db owners set 'CDDO'                 --organisation co-cddo
dmd --db dmd.db owners set 'Elastic'              --organisation elastic
```


This will then work through the repos in each known [datasource](/concepts/datasource/) and update the [`owners` table](/schema/#internalownershipdbschemasql) accordingly.

**Note** that this should be run _after_ any repos are imported.

### Bulk import a CSV file

Alternatively, shelling out for each repo can be a little awkward, so we can instead produce a CSV, such as:

```csv
# note that no header is required
github,elastic,kibana,Elastic,
gitlab,jamietanna,micropub-go,Jamie Tanna,Notes can be added too
```

Then we can import this with

```sh
dmd --db dmd.db owners import owners.csv
```

Note that when using the bulk import method, you [can't use](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/212) wildcard characters.

## Using ownership data

This then allows you to write queries such as the below to find repos using the Renovate [datasource](/concepts/datasource/) with their ownership information ([example web link](https://dependency-management-data-example.fly.dev/datasette/dmd?sql=select%0D%0A++distinct+renovate.platform%2C%0D%0A++renovate.organisation%2C%0D%0A++renovate.repo%2C%0D%0A++owner%2C%0D%0A++notes%0D%0Afrom%0D%0A++renovate%0D%0A++left+join+owners+on+renovate.platform+%3D+owners.platform%0D%0A++and+renovate.organisation+%3D+owners.organisation%0D%0A++and+renovate.repo+%3D+owners.repo)).

```sql
select
  distinct renovate.platform,
  renovate.organisation,
  renovate.repo,
  owner,
  notes
from
  renovate
  left join owners on renovate.platform = owners.platform
  and renovate.organisation = owners.organisation
  and renovate.repo = owners.repo
```

This then allows you to write queries such as the below to find repos using the SBOM [datasource](/concepts/datasource/) with their ownership information ([example web link](https://dependency-management-data-example.fly.dev/datasette/dmd?sql=select%0D%0A++distinct+sboms.platform%2C%0D%0A++sboms.organisation%2C%0D%0A++sboms.repo%2C%0D%0A++owner%2C%0D%0A++notes%0D%0Afrom%0D%0A++sboms%0D%0A++left+join+owners+on+sboms.platform+%3D+owners.platform%0D%0A++and+sboms.organisation+%3D+owners.organisation%0D%0A++and+sboms.repo+%3D+owners.repo)).

```sql
select
  distinct sboms.platform,
  sboms.organisation,
  sboms.repo,
  owner,
  notes
from
  sboms
  left join owners on sboms.platform = owners.platform
  and sboms.organisation = owners.organisation
  and sboms.repo = owners.repo
```
