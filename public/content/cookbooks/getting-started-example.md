---
title: Getting Started (with the example data)
description: How to get started with dependency-management-data, using the pre-collected example data.
weight: 10
---
If you don't (yet) want to use DMD with your own data, it's best to check out the <a href="/example/">example project</a>, which provides some a pre-seeded database, and the associated data exports that you can test the experience with, as well as seeing how it has configured the tooling to build the database.

You can download the example database from the [latest GitLab CI build](https://gitlab.com/tanna.dev/dependency-management-data-example/-/pipelines/latest?ref=main) and download the artifacts from the `build-database` job.

Alternatively, you can use the `dmd-example` CLI:

```sh
$ go install gitlab.com/tanna.dev/dependency-management-data-example/cmd/dmd-example@latest
$ dmd-example download
Successfully downloaded `dmd.db` (from the last run at dmd.db) to `2023-04-20 09:42:37.498 +0000 UTC`
$ ls dmd.db
dmd.db
$ dmd-example shell
# SQLite version 3.39.5 2022-10-14 20:58:05
# Enter ".help" for usage hints.
# sqlite>
```

The example data has a corresponding deployment of the DMD web application at <a href="https://dependency-management-data-example.fly.dev/">dependency-management-data-example.fly.dev</a>.

Some examples of queries:

<ul>
<li>Show the use of unstable versions - <code>select * from renovate where version like '0.%' or version like
'v0.%';</code> (<a
    href="https://dependency-management-data-example.fly.dev/datasette/dmd?sql=select+*+from+renovate+where+version+like+%270.%25%27+or+version+like+%27v0.%25%27%3B">web
    link</a>)</li>
<li>Show the list of packages by package manager - <code>SELECT package_manager, count(*) from renovate group by
package_manager order by count(*) desc;</code> (<a
    href="https://dependency-management-data-example.fly.dev/datasette/dmd?sql=SELECT+package_manager%2C+count%28*%29+from+renovate+group+by+package_manager+order+by+count%28*%29+desc%3B">web
    link</a>)</li>
</ul>

