---
title: Getting Started
description: How to get started with dependency-management-data for package data.
weight: 1
---
## TL;DR extraordinaire

At a minimum, you need to:

- install the `dmd` CLI
- retrieve some data
  - the options available can be found in the [Data Collection Patterns cookbook](/cookbooks/collecting-data/)
- create the SQLite database for dependency-management-data
- import the data

We can do this by running:

```sh
go install dmd.tanna.dev/cmd/dmd@latest

# produce some data that DMD can import, for instance via renovate-graph
# note that the GITHUB_TOKEN must be a fine-grained, or classic, Personal Access Token with either `repo` or `public_repo` scopes
npx @jamietanna/renovate-graph@latest --token $GITHUB_TOKEN your-org/repo another-org/repo
# or for GitLab
env RENOVATE_PLATFORM=gitlab npx @jamietanna/renovate-graph@latest --token $GITLAB_TOKEN your-org/repo another-org/nested/repo

# set up the database
dmd db init --db dmd.db
# import renovate-graph data
dmd import renovate --db dmd.db 'out/*.json'
# then you can start querying it
sqlite3 dmd.db 'select count(*) from renovate'
```

## Installing the `dmd` CLI

Pre-built releases can be downloaded from [the GitLab Package Registry](https://gitlab.com/tanna.dev/dependency-management-data/-/packages).

Alternatively, you can install it from source, provided you have [a Go toolchain](https://go.dev/doc/install):

```sh
go install dmd.tanna.dev/cmd/dmd@latest
```

## Retrieving the data

As noted above, we need to retrieve data to be imported into DMD.

There are several options available, and to choose the best option for you, check out the [Data Collection Patterns cookbook](/cookbooks/collecting-data/).

In this example, we'll use use [renovate-graph](https://gitlab.com/tanna.dev/renovate-graph), which uses [Renovate](https://docs.renovatebot.com) as the engine for retrieving package data.

We can run the following:

```sh
# optional, allows renovate-graph to retrieve the `current_version` column, as well as populate the `renovate_updates` table
export RG_INCLUDE_UPDATES='true'

# produce some data that DMD can import, for instance via renovate-graph
# note that the GITHUB_TOKEN must be a fine-grained, or classic, Personal Access Token with either `repo` or `public_repo` scopes
npx @jamietanna/renovate-graph@latest --token $GITHUB_TOKEN jamietanna/jamietanna deepmap/oapi-codegen
# or for GitLab
env RENOVATE_PLATFORM=gitlab npx @jamietanna/renovate-graph@latest --token $GITLAB_TOKEN tanna.dev/serve jamietanna/tidied
```

## Creating the database and importing the data

Once `renovate-graph` has executed, you'll see an `out` directory with one file per repo.

First, we'll create the database:

```sh
# or any name, really
dmd db init --db dmd.db
```

Then, we need to import the data. Notice the quotes around the argument to avoid shell globbing

```sh
dmd import renovate --db dmd.db 'out/*.json'
```

Now our database is ready to go 👏

## Generating missing data (optional)

This is an optional step, but for ecosystems like Java, the full dependency tree may not be immediately available.

We can run the following to (try) to fill in the missing dependency tree:

```sh
# note that this can take several minutes depending on how many dependencies you have!
dmd db generate missing-data --db dmd.db
```

## Generating advisories (optional)

This is an optional step, but allows us to get some more meaningful information about our dependencies.

We can run the following to set up our [advisories](/concepts/advisory):

```sh
# optionally fetch community-sourced custom advisories
dmd contrib download

# then generate advisories for all our packages
# note that this can take several minutes depending on how many dependencies you have!
dmd db generate advisories --db dmd.db
```

## Running some queries

Now we've got the data available, we can start to query it.

It's recommended you find your SQLite browser of choice and try the following queries:

```sql
-- how many packages have been ingested via renovate-graph
select count(*) from renovate

-- how many pending package updates have been ingested via renovate-graph
select count(*) from renovate_updates

-- how many packages have been ingested via dependabot-graph or through SBOM imports
select count(*) from sboms

-- what are your most popular 10 transitive Go dependencies?
select
  distinct package_name,
  count(*)
from
  renovate,
  json_each(dep_types) as dep_type
where
  package_manager = 'gomod'
  and dep_type.value = 'indirect'
group by
  package_name
order by
  count(*) DESC
limit 10;

```

And from the `dmd` CLI, we can also run the following:

```sh
# if you've generated the advisories data
dmd report advisories --db dmd.db

dmd report mostPopularDockerImages --db dmd.db
dmd report mostPopularPackageManagers --db dmd.db
```
