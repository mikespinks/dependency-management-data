---
title: Report
description: A pre-configured query to flag specific package data.
---
Reports are a way to get a pre-configured view of specific information in your database.

The existing reports and what [datasources](/concepts/datasource/) they support can be found [on the features page](/features/#reports).

Reports can be run [on the command-line](/commands/dmd_report) or [via the web UI](/web/).

These definitely won't match all the queries your organisation wants to use, so [in the future](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/130) it'll be easier to save queries natively in `dmd-web`.
