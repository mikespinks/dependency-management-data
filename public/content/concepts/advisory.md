---
title: Advisory
description: A way to flag the usage of a given package as potentially risky, or at least requiring review.
---
Advisories are a great way to get insight into packages that are being used and flag cases where packages in use may not be wanted.

For instance, by [generating advisories](/commands/dmd_db_generate_advisories/), we can get insight into:

- Dependencies that are running deprecated/end-of-life software
- Dependencies that are potentially affected by Common Vulnerabilities and Exploits (CVEs)

As noted in [Custom Advisories: the unsung hero of dependency-management-data](https://www.jvt.me/posts/2023/08/29/dmd-custom-advisories/), it's also possible to add your own advisories to flag packages that your organisation may not want to use, highlight versions of internal libraries that have security issues, and many other possibilities.

You can see the supported types of advisories defined in the database schema for [the custom advisories](/schema/#internaladvisorydbschemasql) table.

**Note** that other tables are used for generating and determining advisories other than the `advisories` table.
