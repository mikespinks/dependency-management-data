---
title: Datasource
description: A source of data that can be queried by dependency-management-data.
---
Currently, we have the following datasources:

## Renovate

- Produced by: [Renovate](https://docs.renovatebot.com/) either via [renovate-graph](https://gitlab.com/tanna.dev/renovate-graph/) or via Renovate's debug logs
- Imported using: [`dmd import renovate`](/commands/dmd_import_renovate/)
- [Database schema](/schema/#internaldatasourcesrenovatedbschemasql)

## Software Bill of Materials (SBOMs)

- Produced by: Various Software Composition Analysis (SCA) tools, such as Snyk, or via [dependabot-graph](https://gitlab.com/tanna.dev/dependabot-graph/)
- Imported using: [`dmd import sbom`](/commands/dmd_import_sbom/)
- [Database schema](/schema/#internaldatasourcessbomdbschemasql)

## Dependabot

<div class="card warn">
<p>The Dependabot datasource is deprecated, and has been replaced with the SBOMs datasource as of dependency-management-data v0.38.0.</p>

<p>The data provided by the latest versions of <a href=https://gitlab.com/tanna.dev/dependabot-graph><code>dependabot-graph</code></a> are superior to the previous version that populated the <code>dependabot</code> table, and is fully integrated in with the SBOM functionality built into DMD.</p>

<p>This will be <a href="https://gitlab.com/tanna.dev/dependency-management-data/-/issues/168">removed soon</a>.</p>
</div>

## AWS Infrastructure

### AWS Elasticache

- Produced by: [endoflife-checker](https://gitlab.com/tanna.dev/endoflife-checker)
- Imported using: [`dmd import awselasticache`](/commands/dmd_import_awslambda/)
- [Database schema](/schema/#internaldatasourcesawselasticachedbschemasql)

### AWS Lambda

- Produced by: [endoflife-checker](https://gitlab.com/tanna.dev/endoflife-checker)
- Imported using: [`dmd import awslambda`](/commands/dmd_import_awslambda/)
- [Database schema](/schema/#internaldatasourcesawslambdadbschemasql)

### AWS RDS

- Produced by: [endoflife-checker](https://gitlab.com/tanna.dev/endoflife-checker)
- Imported using: [`dmd import awsrds`](/commands/dmd_import_awsrds/)
- [Database schema](/schema/#internaldatasourcesawsrdsdbschemasql)
