---
title: Components
---
DMD consists of several components:

- [`dmd` CLI](/commands/dmd/)
- [`dmd-web` web application](/web/)
- [Database schema for the underlying SQLite database](/schema/)
- [-contrib repository](https://gitlab.com/tanna.dev/dependency-management-data-contrib) which allows community-sourced configuration and snippets
- [Example repository](https://gitlab.com/tanna.dev/dependency-management-data-example/) which is deployed to [dependency-management-data-example.fly.dev](https://dependency-management-data-example.fly.dev/)
