package cmd

import (
	"database/sql"
	"fmt"
	"os"

	"dmd.tanna.dev/internal/datasources/awsrds"
	"dmd.tanna.dev/internal/view"
	"github.com/spf13/cobra"
)

var importAwsrdsCmd = &cobra.Command{
	Use:   "awsrds '/path/to/*.json'",
	Short: "Import a data dump from aws-rds-endoflife ",
	Long: `Takes a data export from https://gitlab.com/tanna.dev/endoflife-checker's command aws-rds-endoflife checker and converts it to the database model.

Example usage:

{dmd} import awsrds '../out/*.json' --db out.db
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cobra.CheckErr(fmt.Errorf("Missing argument"))
		}

		db, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		pw := view.NewProgressWriter(os.Stdout, noProgress)

		parser := awsrds.NewParser()
		databases, err := parser.ParseFiles(args[0], pw)
		cobra.CheckErr(err)

		importer := awsrds.NewImporter()
		err = importer.ImportDatabases(cmd.Context(), databases, db, pw)
		cobra.CheckErr(err)
	},
}

func init() {
	importCmd.AddCommand(importAwsrdsCmd)
	addNoProgressFlag(importAwsrdsCmd)
}
