package cmd

import (
	"database/sql"
	"os"

	"dmd.tanna.dev/internal/contrib"
	"dmd.tanna.dev/internal/view"
	"github.com/spf13/cobra"
)

var contribSyncCmd = &cobra.Command{
	Use:   "sync",
	Short: "Sync data from the -contrib repo to the database",
	Long: `Sync the downloaded data from the https://gitlab.com/tanna.dev/dependency-management-data-contrib repository to the database

Requires you run ` + "`dmd contrib download`" + ` first

Note that this will allow for arbitrary SQL to be executed - it's worth reviewing the queries if you are concerned about this.
`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		pw := view.NewProgressWriter(os.Stdout, noProgress)

		err = contrib.Sync(cmd.Context(), logger, sqlDB, pw)
		cobra.CheckErr(err)
	},
}

func init() {
	contribCmd.AddCommand(contribSyncCmd)
}
