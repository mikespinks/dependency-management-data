package cmd

import (
	"database/sql"
	"fmt"

	"dmd.tanna.dev/internal/licenses"
	"github.com/spf13/cobra"
)

var reportLicensesCmd = &cobra.Command{
	Use:     "licenses",
	Aliases: []string{"licences"},
	Short:   "Report license information for package dependencies",
	Long: `Report license information for package dependencies

This report utilises data from deps.dev

Requires running ` + "`" + `db generate advisories` + "`" + ` to seed the data.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		tw, err := licenses.ReportLicensingOverview(cmd.Context(), logger, sqlDB, platform, organisation, repo, owner)
		cobra.CheckErr(err)

		fmt.Println(tw.Render())
	},
}

func init() {
	reportCmd.AddCommand(reportLicensesCmd)
	reportLicensesCmd.Flags().StringVar(&platform, "platform", "", "")
	reportLicensesCmd.Flags().StringVar(&organisation, "organisation", "", "")
	reportLicensesCmd.Flags().StringVar(&repo, "repo", "", "")
	reportLicensesCmd.Flags().StringVar(&owner, "owner", "", "")
}
