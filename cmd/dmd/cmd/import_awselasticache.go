package cmd

import (
	"database/sql"
	"fmt"
	"os"

	"dmd.tanna.dev/internal/datasources/awselasticache"
	"dmd.tanna.dev/internal/view"
	"github.com/spf13/cobra"
)

var importAwselasticacheCmd = &cobra.Command{
	Use:   "awselasticache '/path/to/*.json'",
	Short: "Import a data dump from aws-elasticache-endoflife ",
	Long: `Takes a data export from https://gitlab.com/tanna.dev/endoflife-checker's command aws-elasticache-endoflife checker and converts it to the database model.

Example usage:

{dmd} import awselasticache '../out/*.json' --db out.db
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cobra.CheckErr(fmt.Errorf("Missing argument"))
		}

		db, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		pw := view.NewProgressWriter(os.Stdout, noProgress)

		parser := awselasticache.NewParser()
		datastores, err := parser.ParseFiles(args[0], pw)
		cobra.CheckErr(err)

		importer := awselasticache.NewImporter()
		err = importer.ImportDatastores(cmd.Context(), datastores, db, pw)
		cobra.CheckErr(err)
	},
}

func init() {
	importCmd.AddCommand(importAwselasticacheCmd)
	addNoProgressFlag(importAwselasticacheCmd)
}
