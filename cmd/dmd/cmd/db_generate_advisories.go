package cmd

import (
	"database/sql"
	"os"

	"dmd.tanna.dev/internal/contrib"
	"dmd.tanna.dev/internal/datasources/awselasticache"
	"dmd.tanna.dev/internal/datasources/awslambda"
	"dmd.tanna.dev/internal/datasources/awsrds"
	"dmd.tanna.dev/internal/depsdev"
	"dmd.tanna.dev/internal/endoflifedate"
	"dmd.tanna.dev/internal/osvdev"
	"dmd.tanna.dev/internal/view"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/spf13/cobra"
)

var dbGenerateAdvisoriesCmd = &cobra.Command{
	Use:   "advisories",
	Short: "Seed the database with known package advisories",
	Long: `Seed the database with known advisories about packages' security or maintainence posture.

This uses information available in the Open Source ecosystem about known unmaintained packages or packages that are marked as deprecated and provides a free-form field to specify some reasoning as to why the advisory is present, and any remediation steps if necessary.

This determines whether the packages you are using are running/approaching End Of Life (EOL) versions, through integration with:

- EndOfLife.date (https://endoflife.date)

This includes the generation of licensing information (to determine i.e. "how many packages use AGPL-3.0 licensed code") as well as Common Vulnerabilities and Exposures (CVE) information, and integrates with:

- Open Source Vulnerability API (https://osv.dev)
- deps.dev (https://deps.dev)

Note that this may lead to the leakage of package names to external systems, which may be seen as a privacy or security issue, and will be worked on as part of https://gitlab.com/tanna.dev/dependency-management-data/-/issues/58.

Known issues:

- Renovate data missing https://gitlab.com/tanna.dev/dependency-management-data/-/issues/77
`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		httpClient := retryablehttp.NewClient().HTTPClient

		pw := view.NewProgressWriter(os.Stdout, noProgress)

		err = depsdev.Generate(cmd.Context(), logger, sqlDB, pw, httpClient)
		cobra.CheckErr(err)

		err = osvdev.Generate(cmd.Context(), logger, sqlDB, pw, httpClient)
		cobra.CheckErr(err)

		err = contrib.GenerateAdvisories(cmd.Context(), logger, sqlDB, pw)
		cobra.CheckErr(err)

		err = endoflifedate.Generate(cmd.Context(), logger, sqlDB, pw, httpClient)
		cobra.CheckErr(err)

		err = awslambda.GenerateEndOfLife(cmd.Context(), logger, sqlDB, pw)
		cobra.CheckErr(err)

		err = awsrds.GenerateEndOfLife(cmd.Context(), logger, sqlDB, pw)
		cobra.CheckErr(err)

		err = awselasticache.GenerateEndOfLife(cmd.Context(), logger, sqlDB, pw)
		cobra.CheckErr(err)
	},
}

func init() {
	dbGenerateCmd.AddCommand(dbGenerateAdvisoriesCmd)
	addNoProgressFlag(dbGenerateAdvisoriesCmd)
}
