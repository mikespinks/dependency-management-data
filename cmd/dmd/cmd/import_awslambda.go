package cmd

import (
	"database/sql"
	"fmt"
	"os"

	"dmd.tanna.dev/internal/datasources/awslambda"
	"dmd.tanna.dev/internal/view"
	"github.com/spf13/cobra"
)

var importAwslambdaCmd = &cobra.Command{
	Use:   "awslambda '/path/to/*.json'",
	Short: "Import a data dump from aws-lambda-endoflife ",
	Long: `Takes a data export from https://gitlab.com/tanna.dev/endoflife-checker's command aws-lambda-endoflife checker and converts it to the database model.

Example usage:

{dmd} import awslambda '../out/*.json' --db out.db
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cobra.CheckErr(fmt.Errorf("Missing argument"))
		}

		db, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		pw := view.NewProgressWriter(os.Stdout, noProgress)

		parser := awslambda.NewParser()
		functions, err := parser.ParseFiles(args[0], pw)
		cobra.CheckErr(err)

		importer := awslambda.NewImporter()
		err = importer.ImportFunctions(cmd.Context(), functions, db, pw)
		cobra.CheckErr(err)
	},
}

func init() {
	importCmd.AddCommand(importAwslambdaCmd)
	addNoProgressFlag(importAwslambdaCmd)
}
