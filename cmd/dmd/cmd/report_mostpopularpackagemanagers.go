package cmd

import (
	"database/sql"
	"fmt"

	"dmd.tanna.dev/internal/datasources"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
)

var reportMostPopularPackageManagersCmd = &cobra.Command{
	Use:   "mostPopularPackageManagers",
	Short: "Query the most popular package managers in use",
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		results, err := datasources.QueryMostPopularPackageManagers(cmd.Context(), sqlDB)
		cobra.CheckErr(err)
		warnIfNoResults(results)

		for k, v := range results {
			fmt.Println(k)
			tw := table.NewWriter()
			tw.AppendHeader(table.Row{
				"Package Manager", "#",
			})

			for _, row := range v {
				tw.AppendRow(table.Row{
					row.PackageManager, row.Count,
				})
			}
			fmt.Println(tw.Render())
		}
	},
}

func init() {
	reportCmd.AddCommand(reportMostPopularPackageManagersCmd)
}
