package cmd

import (
	"dmd.tanna.dev/internal/contrib"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
)

var contribDownloadCmd = &cobra.Command{
	Use:   "download",
	Short: "Download data from the -contrib repo ",
	Long: `Download the latest state of the https://gitlab.com/tanna.dev/dependency-management-data-contrib repository

Note that this will allow for arbitrary SQL to be executed - it's worth reviewing the queries if you are concerned about this.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		// as -contrib is a public repo, we can use an unauthenticated client
		glab, err := gitlab.NewClient("")
		cobra.CheckErr(err)

		err = contrib.DownloadContrib(glab, contrib.ContribPath())
		cobra.CheckErr(err)
	},
}

func init() {
	contribCmd.AddCommand(contribDownloadCmd)
}
