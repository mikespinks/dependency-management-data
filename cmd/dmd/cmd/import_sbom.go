package cmd

import (
	"database/sql"
	"fmt"
	"os"

	"dmd.tanna.dev/internal/datasources/sbom"
	"dmd.tanna.dev/internal/view"
	"github.com/spf13/cobra"
)

var importSBOMCmd = &cobra.Command{
	Use:   "sbom '/path/to/sbom.json' --platform github --organisation jamietanna --repo jamietanna",
	Short: "Import an SBOM",
	Long: `Imports a Software Bill of Materials (SBOM).

Example usage:

{dmd} import sbom '/path/to/sbom.json' --platform github --organisation jamietanna --repo jamietanna --db dmd.db
{dmd} import sbom '/path/to/sbom.json' --platform gitlab --organisation tanna.dev --repo dependency-management-data --db dmd.db
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cobra.CheckErr(fmt.Errorf("Missing argument"))
		}

		db, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		pw := view.NewProgressWriter(os.Stdout, noProgress)

		parser := sbom.NewParser()
		deps, err := parser.ParseFile(args[0], platform, organisation, repo)
		cobra.CheckErr(err)

		importer := sbom.NewImporter()
		err = importer.ImportDependencies(cmd.Context(), deps, db, pw)
		cobra.CheckErr(err)
	},
}

func init() {
	importCmd.AddCommand(importSBOMCmd)
	addNoProgressFlag(importSBOMCmd)

	importSBOMCmd.Flags().StringVar(&platform, "platform", "", "")
	cobra.CheckErr(importSBOMCmd.MarkFlagRequired("platform"))

	importSBOMCmd.Flags().StringVar(&organisation, "organisation", "", "")
	cobra.CheckErr(importSBOMCmd.MarkFlagRequired("organisation"))

	importSBOMCmd.Flags().StringVar(&repo, "repo", "", "")
	cobra.CheckErr(importSBOMCmd.MarkFlagRequired("repo"))
}
