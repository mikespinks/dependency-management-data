package cmd

import (
	"database/sql"
	"fmt"
	"os"
	"path/filepath"

	"dmd.tanna.dev/internal/advisory"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
)

var reportAdvisoriesCmd = &cobra.Command{
	Use:   "advisories",
	Short: "Report advisories that are available for packages or dependencies in use",
	Long: `Report advisories that are available for packages or dependencies in use

This reports advisories from the following sources:

- the ` + "`" + `advisories` + "`" + ` table for custom or community-informed advisories
- End of Life package advisories, via endoflife.date, and highlights whether dependencies are lacking active support or are actively end-of-life.
- AWS infrastructure version advisories, via endoflife-checker

Requires running ` + "`" + `db generate advisories` + "`" + ` to seed the data.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		tw, err := advisory.ReportPackages(cmd.Context(), logger, sqlDB, platform, organisation, repo, owner)
		cobra.CheckErr(err)
		err = outputTable(tw, "package-advisories.csv")
		cobra.CheckErr(err)

		tw, err = advisory.ReportAWS(cmd.Context(), logger, sqlDB)
		cobra.CheckErr(err)
		err = outputTable(tw, "aws-advisories.csv")
		cobra.CheckErr(err)
	},
}

func outputTable(tw table.Writer, reportName string) error {
	if csvOutput {
		err := os.MkdirAll(outPath, 0700)
		if err != nil {
			return err
		}

		err = os.WriteFile(filepath.Join(outPath, reportName), []byte(tw.RenderCSV()), 0600)
		if err != nil {
			return err
		}

		return nil
	}

	fmt.Println(tw.Render())
	return nil
}

func init() {
	reportCmd.AddCommand(reportAdvisoriesCmd)
	reportAdvisoriesCmd.Flags().BoolVar(&csvOutput, "csv", false, "Whether to output as a CSV file")
	reportAdvisoriesCmd.Flags().StringVar(&outPath, "out", "", "Where to output report(s)")
	reportAdvisoriesCmd.MarkFlagsRequiredTogether("csv", "out")
	reportAdvisoriesCmd.Flags().StringVar(&platform, "platform", "", "")
	reportAdvisoriesCmd.Flags().StringVar(&organisation, "organisation", "", "")
	reportAdvisoriesCmd.Flags().StringVar(&repo, "repo", "", "")
	reportAdvisoriesCmd.Flags().StringVar(&owner, "owner", "", "")
}
