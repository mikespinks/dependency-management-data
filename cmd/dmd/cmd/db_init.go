package cmd

import (
	"context"
	"database/sql"
	"fmt"
	"os"

	"dmd.tanna.dev/internal/metadata/db"
	"dmd.tanna.dev/internal/repositories"
	"github.com/spf13/cobra"
)

var dbInitCmd = &cobra.Command{
	Use:   "init",
	Short: "Initialise the local database",
	Long:  `Initialise the local database before importing data into it.`,
	Run: func(cmd *cobra.Command, args []string) {
		warnIfDatabaseExists(cmd.Context(), databasePath)

		sqlDB, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		err = repositories.CreateTables(cmd.Context(), sqlDB)
		cobra.CheckErr(err)

		metadata := db.New(sqlDB)
		err = metadata.SetDMDVersion(cmd.Context(), versionInfo.short)
		cobra.CheckErr(err)

		fmt.Println("Successfully initialised", databasePath)
	},
}

func init() {
	dbCmd.AddCommand(dbInitCmd)
}

func warnIfDatabaseExists(ctx context.Context, databasePath string) {
	if _, err := os.Stat(databasePath); err != nil {
		return
	}

	sqlDB, err := sql.Open("sqlite", databasePath)
	if err != nil {
		return
	}

	metadata := db.New(sqlDB)
	m, err := metadata.RetrieveDMDVersion(ctx)
	if err == nil {
		logger.Warn(fmt.Sprintf("The database %s has already been initialised, so re-initialising it may not work, as `dmd` does not perform migrations", databasePath), "dmdVersion", m.Value)
	} else {
		logger.Warn(fmt.Sprintf("The file %s already exists, so re-initialising it may not work, as `dmd` does not perform migrations", databasePath))
	}
}
