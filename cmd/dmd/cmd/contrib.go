package cmd

import (
	"github.com/spf13/cobra"
)

var contribCmd = &cobra.Command{
	Use:   "contrib",
	Short: "Manage data from the -contrib repo",
	Long:  `A set of commands to manage the data from the https://gitlab.com/tanna.dev/dependency-management-data-contrib repository, which allows user-provided content.`,
}

func init() {
	rootCmd.AddCommand(contribCmd)
}
