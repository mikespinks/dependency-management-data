package cmd

import (
	"database/sql"
	"fmt"
	"io/ioutil"

	"dmd.tanna.dev/internal/repositories"
	"dmd.tanna.dev/pkg/sqlitehelpers"
	"github.com/spf13/cobra"
)

var orgs []string

var dbAnonymiseCmd = &cobra.Command{
	Use:     "anonymise",
	Aliases: []string{"anonymize"},
	Short:   "Anonymise data in the database to allow sharing parts of the database",
	Long: `Anonymise data in the database to allow sharing parts of the database, for instance when reporting a bug, or demonstrating the data set without giving too much information around which projects use which dependencies.

*NOTE* that this is not truly anonymised. It uses a trivial means to anonymise the data which cannot be reverse engineered, but someone could fairly easily brute-force the data.

It is recommended that you understand that providing the data is not truly anonymous and make steps to further anonymise it if required.

As well as the base anonymisation process, whcih removes the ` + "`" + `organisation` + "`" + ` and ` + "`" + `repo` + "`" + ` fields, we can also further remove references by providing the ` + "`" + `--orgs` + "`" + ` argument.

For instance, if we wanted to further anonymise any references to ` + "`" + `jenkinsci` + "`" + ` and ` + "`" + `tanna.dev` + "`" + ` inside package names, package file paths and versions, we could run:

    dmd db anonymise --db dmd.db --out dmd-anon.db --orgs jenkinsci,tanna.dev
	`,
	Run: func(cmd *cobra.Command, args []string) {
		bytes, err := ioutil.ReadFile(databasePath)
		cobra.CheckErr(err)

		err = ioutil.WriteFile(outPath, bytes, 0644)
		cobra.CheckErr(err)

		sqlDB, err := sql.Open("sqlite", outPath)
		cobra.CheckErr(err)

		err = repositories.AnonymiseData(cmd.Context(), sqlDB, orgs)
		cobra.CheckErr(err)

		if len(orgs) == 0 {
			fmt.Printf("Successfully anonymised all data inside %s\n", outPath)
			fmt.Println(`Note that this may not be an exhaustive set of data scrubbing, and before sharing this anonymised output, verify that no sensitive references to your organisation are present`)
			fmt.Println(`See also the notice in the --help output`)
			return
		}

		fmt.Printf("Successfully anonymised all data inside %s according to orgs: %v\n", outPath, orgs)
		fmt.Println(`Note that this may not be an exhaustive set of data scrubbing, and before sharing this anonymised output, try running i.e.:

sqlite3 ` + outPath + ` .dump | grep ` + orgs[0] + `

And verifying that no output is found`)
		fmt.Println(`See also the notice in the --help output`)
	},
}

func init() {
	dbCmd.AddCommand(dbAnonymiseCmd)

	dbAnonymiseCmd.Flags().StringVar(&outPath, "out", "", "the path to anonymised output database")
	dbAnonymiseCmd.MarkFlagRequired("out")

	dbAnonymiseCmd.Flags().StringSliceVar(&orgs, "orgs", nil, "a comma-separated list of `organisation`s that should be ?")

	sqlitehelpers.RegisterSha3()
}
