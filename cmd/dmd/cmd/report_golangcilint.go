package cmd

import (
	"database/sql"
	"fmt"

	"dmd.tanna.dev/internal/datasources"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
)

var reportGolangCILint = &cobra.Command{
	Use:   "golangCILint",
	Short: "Query usages of golangci-lint, tracked as a source-based dependency",
	Long: `Query usages of golangci-lint, tracked as a source-based dependency

Tracking golangci-lint as a source-based dependency, so you can i.e. ` + "`" + `go run` + "`" + ` is discouraged, as it can impact the dependency tree, i.e. clashing with existing dependencies, as well as growing the dependency tree considerably.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		results, err := datasources.QueryGolangCILint(cmd.Context(), sqlDB)
		cobra.CheckErr(err)
		warnIfNoResults(results)

		for k, v := range results {
			fmt.Println(k)

			if len(v.Direct) == 0 && len(v.Indirect) == 0 {
				fmt.Println("No source-tracked dependencies on golangci-lint")
			} else {

				if len(v.Direct) == 0 {
					fmt.Println("No direct dependencies found on golangci-lint")
				} else {
					fmt.Printf("Direct dependencies found in %d repos\n", len(v.Direct))

					tw := table.NewWriter()

					tw.AppendHeader(table.Row{
						"Platform", "Organisation", "Repo", "Owner",
					})

					for _, dep := range v.Direct {
						tw.AppendRow(table.Row{
							dep.Platform, dep.Organisation, dep.Repo, stringPointerToString(dep.Owner),
						})
					}

					fmt.Println(tw.Render())
				}

				if len(v.Indirect) == 0 {
					fmt.Println("No indirect dependencies found on golangci-lint")
				} else {
					fmt.Printf("Indirect dependencies found in %d repos\n", len(v.Indirect))

					tw := table.NewWriter()

					tw.AppendHeader(table.Row{
						"Platform", "Organisation", "Repo", "Owner",
					})

					for _, dep := range v.Indirect {
						tw.AppendRow(table.Row{
							dep.Platform, dep.Organisation, dep.Repo, stringPointerToString(dep.Owner),
						})
					}

					fmt.Println(tw.Render())
				}
			}
		}
	},
}

func stringPointerToString(s *string) string {
	if s == nil {
		return ""
	}
	return *s
}

func init() {
	reportCmd.AddCommand(reportGolangCILint)
}
