package cmd

import (
	"github.com/spf13/cobra"
)

var importCmd = &cobra.Command{
	Use:   "import",
	Short: "Import raw data exports into a given database",
	Long: `Import data exports that are compiled through other means into the provided database.

See the subcommands for information on what's supported.`,
}

func init() {
	rootCmd.AddCommand(importCmd)
	addRequiredDbFlag(importCmd)
}
