package main

import (
	"dmd.tanna.dev/cmd/dmd/cmd"

	"github.com/carlmjohnson/versioninfo"
	_ "modernc.org/sqlite"
)

func main() {
	cmd.SetVersionInfo(versioninfo.Version, versioninfo.Revision, versioninfo.Short())
	cmd.Execute()
}
