package main

import (
	_ "embed"
	"flag"
	"fmt"
	"log"
	"os"
	"text/template"

	"github.com/iancoleman/strcase"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

//go:embed renovate.tmpl
var renovateTmpl string

//go:embed sbom.tmpl
var sbomTmpl string

var titleCaser = cases.Title(language.BritishEnglish)

func main() {
	var product string
	var datasource string

	flag.StringVar(&product, "product", "", "The Product according to EndOfLife.date")
	flag.StringVar(&datasource, "datasource", "", "The DMD datasource")

	flag.Parse()

	var templateString string

	switch datasource {
	case "renovate":
		templateString = renovateTmpl
	case "sbom":
		templateString = sbomTmpl
	}

	if templateString == "" {
		log.Fatal("Invalid datasource parameter provided")
	}

	if product == "" {
		log.Fatal("Invalid product parameter provided")
	}

	data := map[string]any{
		"Product":          product,
		"ParserStructName": fmt.Sprintf("%sParser%s", datasource, strcase.ToCamel(product)),
	}

	filename := fmt.Sprintf("internal/endoflifedate/%s_parser_%s.go", datasource, strcase.ToSnake(product))

	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	must(err)

	tmpl, err := template.New("parser").Parse(templateString)
	must(err)

	err = tmpl.Execute(f, data)
	must(err)

	fmt.Printf("Created initial boilerplate for the `%s` EndOfLife.date product for the `%s` datasource in `%v`. Please see docs/adding_a_new_endoflife_parser.md for more details\n", product, datasource, filename)
}

func must(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
