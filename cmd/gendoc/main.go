package main

import (
	"bytes"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	_ "embed"

	"dmd.tanna.dev/cmd/dmd/cmd"
	"github.com/spf13/cobra/doc"
)

//go:embed schemas.tmpl
var schemasTemplate string

func main() {
	generateSchemaDocumentation()
	generateCommandDocumentation()
}

type schema struct {
	Filename string
	Contents string
}

type schemaTemplateData struct {
	Schemas []schema
}

func generateSchemaDocumentation() {
	filenames, err := globSchemas(".")
	must(err)

	if len(filenames) == 0 {
		log.Fatal("No files named `schema.sql` could be found - `gendoc` may have a bug")
	}

	t := template.Must(template.New("schemas.md").Parse(schemasTemplate))

	schemas := make([]schema, len(filenames))
	for i, v := range filenames {
		b, err := os.ReadFile(v)
		must(err)

		schemas[i] = schema{
			Filename: v,
			Contents: string(b),
		}
	}

	templateData := schemaTemplateData{
		Schemas: schemas,
	}

	var buf bytes.Buffer

	err = t.Execute(&buf, templateData)
	must(err)

	err = os.WriteFile("public/content/schema.md", buf.Bytes(), 0644)
	must(err)
}

func generateCommandDocumentation() {
	dmdCmd := cmd.Command()

	emptyString := func(_ string) string { return "" }

	err := doc.GenMarkdownTreeCustom(dmdCmd, "public/content/commands/", emptyString, func(s string) string {
		// https://www.jvt.me/posts/2019/11/11/gotcha-netlify-lowercase/
		s = strings.ToLower(s)
		return "../" + strings.ReplaceAll(s, ".md", "")
	})
	must(err)
}

// adapted from https://stackoverflow.com/a/26809999
func globSchemas(dir string) ([]string, error) {
	files := []string{}
	err := filepath.Walk(dir, func(path string, f os.FileInfo, err error) error {
		if strings.HasSuffix(path, "schema.sql") {
			files = append(files, path)
		}
		return nil
	})

	return files, err
}

func must(err error) {
	if err != nil {
		log.Fatalf("There was an unexpected error: %s", err)
	}
}
