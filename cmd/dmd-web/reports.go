package main

import (
	"fmt"
	"html/template"
	"net/http"

	"dmd.tanna.dev/internal/advisory"
	"dmd.tanna.dev/internal/datasources"
	"dmd.tanna.dev/internal/licenses"
	"github.com/jedib0t/go-pretty/v6/table"
)

type Report struct {
	Name        string
	Description string
}

func (s *DMDServer) handleReportRoot(w http.ResponseWriter, r *http.Request) {
	data := map[string]any{
		"Reports": s.reports,
	}

	s.renderTemplate(w, r, data, "templates/pages/report-root.html.tmpl")
}

func (s *DMDServer) registerReport(mux *http.ServeMux, reportRoute string, reportName string, reportDescription string, handlerFunc http.HandlerFunc) {
	if s.reports == nil {
		s.reports = make(map[string]Report)
	}

	s.reports["/report/"+reportRoute] = Report{
		Name:        reportName,
		Description: reportDescription,
	}
	mux.HandleFunc("/report/"+reportRoute, handlerFunc)
}

func (s *DMDServer) handleReportAdvisories(w http.ResponseWriter, r *http.Request) {
	report := `<h2>Package advisories</h2>`

	platform := r.URL.Query().Get("platform")
	organisation := r.URL.Query().Get("organisation")
	repo := r.URL.Query().Get("repo")
	owner := r.URL.Query().Get("owner")

	if platform != "" || organisation != "" || repo != "" || owner != "" {
		report += `
<div class="card hint">
		<p>You are currently filtering by the following querystring parameters:</p>
		`

		tw := table.NewWriter()
		tw.AppendHeader(table.Row{
			"Name", "Value",
		})

		tw.AppendRow(table.Row{"platform", platform})
		tw.AppendRow(table.Row{"organisation", organisation})
		tw.AppendRow(table.Row{"repo", repo})
		tw.AppendRow(table.Row{"owner", owner})

		report += tw.RenderHTML()

		report += `
</div>
		`

	}

	report += `<details>
<summary>Filtering</summary>

<p>
	You can filter the data by using the querystring, for instance:
</p>

<ul>
	<li>
		<a href="/report/advisories?platform=github">Repos on GitHub</a>
	</li>
	<li>
		<a href="/report/advisories?organisation=*tanna*">Repos with an owner that contains the word <code>tanna</code></a>
	</li>
</ul>
</details>`

	tw, err := advisory.ReportPackages(r.Context(), logger, s.sqlDB, platform, organisation, repo, owner)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	report += tw.RenderHTML()

	report += `<h2>AWS Infrastructure advisories</h2>`

	tw, err = advisory.ReportAWS(r.Context(), logger, s.sqlDB)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	report += tw.RenderHTML()

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
	}

	s.renderTemplate(w, r, data, "templates/pages/report.html.tmpl")
}

func (s *DMDServer) handleReportGolangCILint(w http.ResponseWriter, r *http.Request) {
	report := ""

	results, err := datasources.QueryGolangCILint(r.Context(), s.sqlDB)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for k, v := range results {
		report += `<h3>` + k + `</h3>`
		if len(v.Direct) == 0 && len(v.Indirect) == 0 {
			report += "No source-tracked dependencies on golangci-lint"
		} else {

			if len(v.Direct) == 0 {
				report += "No direct dependencies found on golangci-lint"
			} else {
				report += fmt.Sprintf("Direct dependencies found in %d repos\n", len(v.Direct))

				tw := table.NewWriter()

				tw.AppendHeader(table.Row{
					"Platform", "Organisation", "Repo", "Owner",
				})

				for _, dep := range v.Direct {
					tw.AppendRow(table.Row{
						dep.Platform, dep.Organisation, dep.Repo, stringPointerToString(dep.Owner),
					})
				}

				report += tw.RenderHTML()
			}

			if len(v.Indirect) == 0 {
				report += "No indirect dependencies found on golangci-lint"
			} else {
				report += fmt.Sprintf("Indirect dependencies found in %d repos\n", len(v.Indirect))

				tw := table.NewWriter()

				tw.AppendHeader(table.Row{
					"Platform", "Organisation", "Repo", "Owner",
				})

				for _, dep := range v.Indirect {
					tw.AppendRow(table.Row{
						dep.Platform, dep.Organisation, dep.Repo, stringPointerToString(dep.Owner),
					})
				}

				report += tw.RenderHTML()
			}
		}
	}

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
	}

	s.renderTemplate(w, r, data, "templates/pages/report.html.tmpl")
}

func (s *DMDServer) handleReportMostPopularDockerImages(w http.ResponseWriter, r *http.Request) {
	report := ``

	results, err := datasources.QueryMostPopularDockerImages(r.Context(), s.sqlDB)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	rowLimit := 10

	for k, v := range results {
		report += `<h3>` + k + `</h3>`

		namespacesTw := table.NewWriter()
		namespacesTw.AppendHeader(table.Row{
			"Namespace", "#",
		})

		for i, count := range v.Namespaces {
			namespacesTw.AppendRow(table.Row{
				count.Name, count.Count,
			})

			if i > rowLimit {
				break
			}
		}
		report += namespacesTw.RenderHTML()

		imagesTw := table.NewWriter()
		imagesTw.AppendHeader(table.Row{
			"Image", "#",
		})

		for i, count := range v.Images {
			imagesTw.AppendRow(table.Row{
				count.Name, count.Count,
			})

			if i > rowLimit {
				break
			}
		}
		report += imagesTw.RenderHTML()
	}

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
	}

	s.renderTemplate(w, r, data, "templates/pages/report.html.tmpl")
}

func (s *DMDServer) handleReportMostPopularPackageManagers(w http.ResponseWriter, r *http.Request) {
	report := ``

	results, err := datasources.QueryMostPopularPackageManagers(r.Context(), s.sqlDB)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for k, v := range results {
		report += `<h3>` + k + `</h3>`
		tw := table.NewWriter()
		tw.AppendHeader(table.Row{
			"Package Manager", "#",
		})

		for _, row := range v {
			tw.AppendRow(table.Row{
				row.PackageManager, row.Count,
			})
		}
		report += tw.RenderHTML()
	}

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
	}

	s.renderTemplate(w, r, data, "templates/pages/report.html.tmpl")
}

func (s *DMDServer) handleReportLicenses(w http.ResponseWriter, r *http.Request) {
	report := `<h2>Package licenses</h2>`

	platform := r.URL.Query().Get("platform")
	organisation := r.URL.Query().Get("organisation")
	repo := r.URL.Query().Get("repo")
	owner := r.URL.Query().Get("owner")

	if platform != "" || organisation != "" || repo != "" || owner != "" {
		report += `
<div class="card hint">
		<p>You are currently filtering by the following querystring parameters:</p>
		`

		tw := table.NewWriter()
		tw.AppendHeader(table.Row{
			"Name", "Value",
		})

		tw.AppendRow(table.Row{"platform", platform})
		tw.AppendRow(table.Row{"organisation", organisation})
		tw.AppendRow(table.Row{"repo", repo})
		tw.AppendRow(table.Row{"owner", owner})

		report += tw.RenderHTML()

		report += `
</div>
		`

	}

	report += `<details>
<summary>Filtering</summary>

<p>
	You can filter the data by using the querystring, for instance:
</p>

<ul>
	<li>
		<a href="/report/licenses?platform=github">Repos on GitHub</a>
	</li>
	<li>
		<a href="/report/licenses?organisation=*tanna*">Repos with an owner that contains the word <code>tanna</code></a>
	</li>
</ul>
</details>`

	tw, err := licenses.ReportLicensingOverview(r.Context(), logger, s.sqlDB, platform, organisation, repo, owner)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	report += tw.RenderHTML()

	data := map[string]any{
		"Data":   template.HTML(report),
		"Report": s.reports[r.URL.Path],
	}

	s.renderTemplate(w, r, data, "templates/pages/report.html.tmpl")
}

func stringPointerToString(s *string) string {
	if s == nil {
		return ""
	}
	return *s
}
