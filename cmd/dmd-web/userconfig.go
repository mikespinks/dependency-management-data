package main

import "html/template"

type UserConfig struct {
	// Banner controls the display of a prominent banner across the top of every page on the application
	Banner struct {
		// HTML the HTML to render in that banner. Remember to add `<p>`s and/or other tags to wrap the text!
		HTML template.HTML `json:"html"`
	} `json:"banner"`
	// Banner controls the display of text in the footer across every page on the application
	Footer struct {
		// HTML the HTML to render in that footer. Remember to add `<p>`s and/or other tags to wrap the text!
		HTML template.HTML `json:"html"`
	} `json:"footer"`
	// DatabaseMeta defines metadata about the underlying database
	DatabaseMeta struct {
		// GenerationTime defines the time that the database was generated
		GenerationTime string `json:"generation_time"`
	} `json:"database_meta"`

	// AdditionalConfig contains any additional key-value configuration that will be presented as-is
	AdditionalConfig map[string]string `json:"additional_config"`
}
