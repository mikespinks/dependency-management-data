package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"log/slog"
	"os"

	"dmd.tanna.dev/internal/metadata/db"
	"github.com/carlmjohnson/versioninfo"
	"github.com/jamietanna/log"
	_ "modernc.org/sqlite"
)

func getVersion() string {
	v := versioninfo.Short()
	if v == "" {
		return "(unknown)"
	}

	return v
}

func getDBVersion(sqlDB *sql.DB) string {
	q := db.New(sqlDB)
	data, err := q.RetrieveDMDVersion(context.Background())
	if err != nil {
		logger.Error(fmt.Sprintf("Failed to look up DMD CLI version in the metadata table: %v", err))
		return ""
	}

	return data.Value
}

var logger = slog.New(log.New(os.Stderr))

type Config struct {
	databasePath string
	port         string
	datasette    DatasetteConfig
	userConfig   UserConfig
}

type DatasetteConfig struct {
	route     string
	extraArgs string
}

func main() {
	ctx := context.Background()

	var config Config
	config.datasette = DatasetteConfig{
		// trailing slash to allow Datasette's static file serving to work
		route: "/datasette/",
	}

	var userConfigPath string

	flag.StringVar(&config.databasePath, "db", "", "Path to the dependency-management-data created SQLite database")
	flag.StringVar(&config.port, "port", "8080", "Local port to bind to")
	flag.StringVar(&userConfigPath, "config", "", "Path to configuration.json file")
	flag.StringVar(&config.datasette.extraArgs, "datasette-extra-args", "", "Extra arguments to be passed to the datasette command via https://docs.datasette.io/en/stable/settings.html#using-setting")
	flag.Parse()

	if config.databasePath == "" {
		logger.Error("Must provide a -db path")
		os.Exit(1)
	}

	if userConfigPath != "" {
		data, err := os.ReadFile(userConfigPath)
		if err != nil {
			log.Fatal(fmt.Sprintf("Failed to open configuration from file %s: %v", userConfigPath, err))
		}
		err = json.Unmarshal(data, &config.userConfig)
		if err != nil {
			log.Fatal(fmt.Sprintf("Failed to parse configuration from file %s as JSON: %v", userConfigPath, err))
		}
	}

	sqlDB, err := sql.Open("sqlite", config.databasePath)
	must(err)

	cmd, datasetteProxy, err := runDatasette(ctx, config.databasePath, config.datasette)
	must(err)

	go func() {
		err = cmd.Run()
		must(err)
	}()

	server := newServer(sqlDB, datasetteProxy, config.datasette.route, config.userConfig)
	server.httpServer.Addr = ":" + config.port

	logger.Info(fmt.Sprintf("Listening on port %s", config.port))

	err = server.httpServer.ListenAndServe()

	// TODO do we need to handle SIGTERM/SIGINT here?
	_, cancelFn := context.WithCancel(ctx)
	cancelFn()

	must(err)
}

func must(err error) {
	if err != nil {
		logger.Error(err.Error(), "err", err)
		os.Exit(1)
	}
}
