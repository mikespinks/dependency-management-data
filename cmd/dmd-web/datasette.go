package main

import (
	"context"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func runDatasette(ctx context.Context, dbPath string, config DatasetteConfig) (*exec.Cmd, *httputil.ReverseProxy, error) {
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		return nil, nil, err
	}
	// on Linux, this port gets allocated, but on MacOS this becomes re-usable (https://stackoverflow.com/a/52004734)
	// so we need to request it and then immediately close it, so we can then reuse the port number
	err = l.Close()
	if err != nil {
		return nil, nil, err
	}

	// a pretty common way of doing this, without handling the possible panic if the type assertion fails
	port := strconv.Itoa(l.Addr().(*net.TCPAddr).Port)

	cmd := "datasette"
	args := []string{
		// use immutable mode to improve caching and performance, via https://docs.datasette.io/en/stable/performance.html
		"-i", dbPath,
		"--port", port,
		// allow reverse proxying correctly
		"--setting", "base_url", config.route,
	}

	extras := strings.Split(config.extraArgs, " ")
	args = append(args, extras...)

	datasetteCmd := exec.CommandContext(ctx,
		cmd, args...,
	)
	datasetteCmd.Stdout = os.Stdout
	datasetteCmd.Stderr = os.Stderr

	u, err := url.Parse("http://127.0.0.1:" + port)
	if err != nil {
		return nil, nil, err
	}

	proxy := httputil.NewSingleHostReverseProxy(u)
	originalDirector := proxy.Director
	proxy.Director = func(req *http.Request) {
		originalDirector(req)
		// This happens on some requests, where we receive
		req.URL.Path = strings.Replace(req.URL.Path, "/datasette/datasette/", "/datasette/", 1)
	}

	return datasetteCmd, proxy, nil
}
