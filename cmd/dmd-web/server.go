package main

import (
	"database/sql"
	"net/http"
	"net/http/httputil"
)

// DMDServer is the Web Application for dependency-management-data (DMD)
type DMDServer struct {
	sqlDB      *sql.DB
	reports    map[string]Report
	httpServer http.Server
	userConfig UserConfig
}

func newServer(sqlDB *sql.DB, datasetteProxy *httputil.ReverseProxy, datasetteProxyPath string, userConfig UserConfig) DMDServer {
	mux := http.NewServeMux()

	server := DMDServer{
		sqlDB:      sqlDB,
		userConfig: userConfig,
	}

	mux.HandleFunc("/", server.handleRoot)
	mux.HandleFunc(datasetteProxyPath, datasetteProxy.ServeHTTP)

	mux.HandleFunc("/report", server.handleReportRoot)
	server.registerReport(mux,
		"advisories", "Advisories", "Report advisories that are available for packages or dependencies in use",
		server.handleReportAdvisories)
	server.registerReport(mux,
		"golangCILint", "golangci-lint", "Query usages of golangci-lint, tracked as a source-based dependency",
		server.handleReportGolangCILint)
	server.registerReport(mux,
		"mostPopularDockerImages", "Most Popular Docker Images", "Query the most popular Docker namespaces and images in use",
		server.handleReportMostPopularDockerImages)
	server.registerReport(mux,
		"mostPopularPackageManagers", "Most Popular Package Managers", "Query the most popular package managers in use",
		server.handleReportMostPopularPackageManagers)
	server.registerReport(mux,
		"licenses", "Licenses", "Report license information for package dependencies",
		server.handleReportLicenses)

	server.httpServer = http.Server{
		Handler: mux,
	}

	return server
}

func (s *DMDServer) handleRoot(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}

	data := map[string]any{}

	s.renderTemplate(w, r, data, "templates/pages/index.html.tmpl")
}
