package main

import (
	"embed"
	"html/template"
	"net/http"
)

//go:embed templates templates/pages templates/partials
var templates embed.FS

func (s *DMDServer) renderTemplate(w http.ResponseWriter, r *http.Request, data map[string]any, filenames ...string) {
	args := []string{
		"templates/base.html.tmpl",
		"templates/partials/head.html.tmpl",
		"templates/partials/header.html.tmpl",
		"templates/partials/footer.html.tmpl",
	}
	args = append(args, filenames...)

	tmpl, err := template.ParseFS(templates, args...)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	data["UserConfig"] = s.userConfig

	data["Meta"] = map[string]string{
		"Version":   getVersion(),
		"DBVersion": getDBVersion(s.sqlDB),
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
