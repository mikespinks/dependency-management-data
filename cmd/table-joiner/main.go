package main

import (
	"bytes"
	_ "embed"
	"log"
	"os"
	"text/template"
)

//go:embed template.sql.tmpl
var sqlTemplate string

type schema struct {
	Filename string
	Contents string
}

type config struct {
	Schemas []schema
}

func must(err error) {
	if err != nil {
		log.Fatalf("There was an unexpected error: %s", err)
	}
}

func main() {
	var toJoin []string
	for i, v := range os.Args {
		if i == 0 {
			continue
		}
		toJoin = append(toJoin, v)
	}

	if len(toJoin) < 2 {
		log.Fatal("Requires more than one argument")
	}

	var config config

	for _, v := range toJoin {
		bytes, err := os.ReadFile(v)
		must(err)

		config.Schemas = append(config.Schemas, schema{
			Filename: v,
			Contents: string(bytes),
		})
	}

	t := template.Must(template.New("schema.sql").Parse(sqlTemplate))

	var buf bytes.Buffer

	err := t.Execute(&buf, config)
	must(err)

	os.WriteFile("joined.sql", buf.Bytes(), 0644)
}
