module dmd.tanna.dev/demos

go 1.21

require github.com/saschagrunert/demo v0.0.0-20231020103811-5fce153baf4b

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/gookit/color v1.5.4 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/urfave/cli/v2 v2.25.7 // indirect
	github.com/xo/terminfo v0.0.0-20210125001918-ca9a967f8778 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/sys v0.13.0 // indirect
)
