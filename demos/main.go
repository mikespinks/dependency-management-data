package main

import (
	"github.com/saschagrunert/demo"
)

func main() {
	d := demo.New()

	d.Add(initAndImport(), "demo-initAndImport", "")

	d.Add(setOwners(), "demo-setOwners", "")

	d.Add(reportGolangCILint(), "demo-reportGolangCILint", "")
	d.Add(reportMostPopularDockerImages(), "demo-mostPopularDockerImages", "")
	d.Add(reportMostPopularPackageManagers(), "demo-mostPopularPackageManagers", "")

	d.Add(generateMissingData(), "demo-generateMissingData", "")

	d.Add(generateAdvisoryAndList(), "demo-generateAdvisoryAndList", "")
	d.Add(reportAdvisories(), "demo-reportAdvisories", "")
	d.Add(reportLicenses(), "demo-reportLicenses", "")

	d.Run()
}

func initAndImport() *demo.Run {
	r := demo.NewRun(
		"Initialise database and import datasources",
	)

	r.Step(nil, demo.S(
		"dmd db init --db dmd.db",
	))

	r.Step(nil, demo.S(
		"dmd import renovate --db dmd.db 'renovate/*.json'",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from renovate'",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from renovate_updates'",
	))

	r.Step(nil, demo.S(
		"dmd import dependabot --db dmd.db 'dependabot/*.json'",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from sboms'",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-bridgy-fed-cyclone.json --platform github --organisation snarfed --repo bridgy-----cyclone",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-bridgy-fed-spdx.json --platform github --organisation snarfed --repo bridgy-----spdx",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-dddem-web-cyclone.json --platform github --organisation DDDEastMidlandsLimited --repo dddem-web-----cyclone",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-dddem-web-spdx.json --platform github --organisation DDDEastMidlandsLimited --repo dddem-web-----spdx",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-pay-webhooks-cyclone.json --platform github --organisation alphagov --repo pay-webhooks-----cyclone",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-pay-webhooks-spdx.json --platform github --organisation alphagov --repo pay-webhooks-----spdx",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-pay-webhooks-container-cyclone.json --platform github --organisation alphagov --repo pay-webhooks-----container-cyclone",
	))

	r.Step(nil, demo.S(
		"dmd import sbom --db dmd.db sbom/snyk-pay-webhooks-container-spdx.json --platform github --organisation alphagov --repo pay-webhooks-----container-spdx",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from sboms'",
	))

	r.Step(nil, demo.S(
		"dmd import awselasticache --db dmd.db 'aws-elasticache/*'",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from aws_elasticache_datastores'",
	))

	r.Step(nil, demo.S(
		"dmd import awslambda --db dmd.db 'aws-lambda/*'",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from aws_lambda_functions'",
	))

	r.Step(nil, demo.S(
		"dmd import awsrds --db dmd.db 'aws-rds/*'",
	))

	r.Step(nil, demo.S(
		"sqlite3 dmd.db 'select count(*) from aws_rds_databases'",
	))

	return r
}

func reportGolangCILint() *demo.Run {
	r := demo.NewRun(
		"Report for usages of golangci-lint which are tracked as a source dependency",
	)

	r.Step(nil, demo.S(
		"dmd report golangCILint --db dmd.db",
	))

	return r
}

func reportMostPopularDockerImages() *demo.Run {
	r := demo.NewRun(
		"Report the most popular Docker namespaces and images",
	)

	r.Step(nil, demo.S(
		"dmd report mostPopularDockerImages --db dmd.db",
	))

	return r
}

func reportMostPopularPackageManagers() *demo.Run {
	r := demo.NewRun(
		"Report the most popular package managers",
	)

	r.Step(nil, demo.S(
		"dmd report mostPopularPackageManagers --db dmd.db",
	))

	return r
}

func setOwners() *demo.Run {
	r := demo.NewRun(
		"Set ownership information",
		"Set bulk ownership information through the `dmd` CLI",
	)

	r.Step(demo.S(
		"Set Jamie Tanna as the owner of all repos in the `tanna.dev` and `jamietanna` organisations",
	), demo.S(
		"dmd owners set --db dmd.db --organisation '*tanna*' 'Jamie Tanna'",
	))

	r.Step(demo.S(
		"Set an owner for a specific organisation and repo",
	), demo.S(
		"dmd owners set --db dmd.db --platform 'github' --organisation jenkinsci --repo job-dsl-plugin 'Job DSL Plugin developers' 'https://github.com/orgs/jenkinsci/teams/job-dsl-plugin-developers'",
	))

	r.StepCanFail(demo.S(
		"Must provide `--platform`, `--organisation` or `--repo`",
	), demo.S(
		"dmd owners set --db dmd.db 'Job DSL Plugin developers'",
	))

	r.Step(demo.S(
		"Show Ownership of repos",
		"Right now, there's no report for this",
	), demo.S(
		"sqlite3 dmd.db 'select platform, organisation, repo, package_name, owner, notes from renovate natural join owners limit 5;'",
	))

	return r
}

func generateMissingData() *demo.Run {
	r := demo.NewRun(
		"Generate missing package data",
		"Via https://osv.dev",
	)

	r.Step(nil, demo.S(
		"dmd db generate missing-data --db dmd.db",
	))

	return r
}

func generateAdvisoryAndList() *demo.Run {
	r := demo.NewRun(
		"Seed the database with known package advisories",
		"Uses the `advisories` table to allow adding custom deprecation/security/etc advisories",
		"Also includes support for:",
		"- End Of Life checking (via https://endoflife.date)",
		"- Common Vulnerabilities and Exposures (CVE) information (via https://osv.dev)",
		"- Licensing and Common Vulnerabilities and Exposures (CVE) information (via https://deps.dev)",
	)

	r.Step(demo.S(
		"Fetch the latest community-contributed data",
	), demo.S(
		"dmd contrib download",
	))

	r.Step(nil, demo.S(
		"dmd db generate advisories --db dmd.db",
	))

	r.Step(demo.S(
		"Show number of CVEs found (via osv.dev)",
		"Right now, there's no report for this",
	), demo.S(
		"sqlite3 dmd.db 'select count(*) from osvdev_cves'",
	))

	r.Step(demo.S(
		"Show CVEs found across (via osv.dev)",
		"Right now, there's no report for this",
	), demo.S(
		"sqlite3 dmd.db 'select platform, organisation, repo, package_name, cve_id from renovate natural join osvdev_cves LIMIT 5'",
	))

	return r
}

func reportAdvisories() *demo.Run {
	r := demo.NewRun(
		"Report advisories",
		"Uses the `advisories` table to allow adding custom deprecation/security/etc advisories",
		"Also includes support for:",
		"- End Of Life checking (via https://endoflife.date)",
		"- Common Vulnerabilities and Exposures (CVE) information (via https://osv.dev)",
		"- Licensing and Common Vulnerabilities and Exposures (CVE) information (via https://deps.dev)",
	)

	r.Step(demo.S(
		"Report advisory information",
	), demo.S(
		"dmd report advisories --db dmd.db",
	))

	r.Step(demo.S(
		"Report advisory information for only projects owned by GDS",
		"Note that the filtering does not currently work on AWS infrastructure advisories",
	), demo.S(
		"dmd report advisories --db dmd.db --owner 'GDS*'",
	))

	r.Step(demo.S(
		"Report advisory information for projects in organisations that contain the word `tanna`",
		"Note that the filtering does not currently work on AWS infrastructure advisories",
	), demo.S(
		"dmd report advisories --db dmd.db --organisation '%tanna*'",
	))

	r.Step(demo.S(
		"Report advisory information for a specific project",
		"Note that the filtering does not currently work on AWS infrastructure advisories",
	), demo.S(
		"dmd report advisories --db dmd.db --platform gitlab --organisation 'tanna.dev' --repo dependency-management-data",
	))

	return r
}

func reportLicenses() *demo.Run {
	r := demo.NewRun(
		"Report licenses",
		"List the licenses in use by dependencies, as determnined by https://deps.dev",
	)

	r.Step(demo.S(
		"Report licensing information",
	), demo.S(
		"dmd report licenses --db dmd.db",
	))

	r.Step(demo.S(
		"Report licensing information for only projects owned by GDS",
	), demo.S(
		"dmd report licenses --db dmd.db --owner 'GDS*'",
	))

	r.Step(demo.S(
		"Report licensing information for projects in organisations that contain the word `tanna`",
	), demo.S(
		"dmd report licenses --db dmd.db --organisation '%tanna*'",
	))

	r.Step(demo.S(
		"Report licensing information for a specific project",
	), demo.S(
		"dmd report licenses --db dmd.db --platform gitlab --organisation 'tanna.dev' --repo dependency-management-data",
	))

	return r
}
