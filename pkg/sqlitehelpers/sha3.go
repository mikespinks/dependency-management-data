package sqlitehelpers

import (
	"database/sql/driver"
	"fmt"

	"golang.org/x/crypto/sha3"
	"modernc.org/sqlite"
)

func RegisterSha3() {
	sqlite.RegisterDeterministicScalarFunction("sha3", 1, sha3Func)
}

func sha3Func(ctx *sqlite.FunctionContext, args []driver.Value) (driver.Value, error) {
	if len(args) != 1 {
		return nil, fmt.Errorf("sha3 requires an argument")
	}
	v, ok := args[0].(string)
	if ok {
		digest := sha3.Sum256([]byte(v))
		ret := make([]byte, 32)
		for i := 0; i < len(digest); i++ {
			ret[i] = digest[i]
		}

		return ret, nil
	}

	return nil, fmt.Errorf("sha3 requires a string argument, got %#v", args[0])
}
