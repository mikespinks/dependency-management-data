package sqlitehelpers

import (
	"database/sql/driver"
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_sha3_Valid(t *testing.T) {
	cases := []struct {
		input string
		// via i.e. select hex(sha3('foo'));
		expectedHex string
	}{
		{
			input:       "foo",
			expectedHex: "76D3BC41C9F588F7FCD0D5BF4718F8F84B1C41B20882703100B9EB9413807C01",
		},
		{
			input:       "bar",
			expectedHex: "CCEEFD7E0545BCF8B6D19F3B5750C8A3EE8350418877BC6FB12E32DE28137355",
		},
		{
			input:       "tanna.dev",
			expectedHex: "06FD72DB6274EA7E655B1AD511DC0ADEC5927D76C21AB142BBE3E54AFB6AFB60",
		},
		{
			input:       "tanna.dev/dependency-management-data",
			expectedHex: "4D73EFD09281EF57EF12963302C66174F2BBBA16353022602F2F4D11368C649F",
		},
	}

	for _, tt := range cases {
		resp, err := sha3Func(nil, []driver.Value{tt.input})
		require.NoError(t, err)

		asHex := strings.ToUpper(fmt.Sprintf("%x", resp))

		assert.Equal(t, tt.expectedHex, asHex)
	}
}
