# Dependency Management Data (DMD)

Dependency Management Data (DMD) is a set of tooling to get a better understanding of the use of dependencies across your organisation.

The aim is to provide you with a set of queryable data about how your projects use configured, so you can target changes across your projects and organisation more appropriately.

There is a more detailed documentation site at [dmd.tanna.dev](https://dmd.tanna.dev).

## Examples + demos

There are [several demos](https://dmd.tanna.dev/example/) available on the documentation site, which provide examples of how DMD works using real data.

The web application can be found at [dependency-management-data-example.fly.dev/](https://dependency-management-data-example.fly.dev/), which uses the same seeded data from the demos.

## Command-line tool

DMD exists as a command-line tool to simplify working with data sources used for dependency-management-data.

The command-line tool has [further documentation available](https://dmd.tanna.dev/commands/dmd/).

### Installation

Pre-built releases can be downloaded from [the GitLab Package Registry](https://gitlab.com/tanna.dev/dependency-management-data/-/packages).

Alternatively, you can install it yourself by running:

```sh
go install dmd.tanna.dev/cmd/dmd@latest
```

### Usage

For instance, if you had used [renovate-graph](https://gitlab.com/tanna.dev/renovate-graph/) to create a directory called `renovate`, you could run:

```sh
dmd db init --db dmd.db
# notice the quoting around the argument, to avoid shell globbing
dmd import renovate --db dmd.db 'renovate/*.json'
```

This will then import the files into `dmd.db` which can then be queried using i.e.:

```
sqlite3 dmd.db
SELECT * from renovate;
```

### Datasources

- Renovate, via [renovate-graph](https://gitlab.com/tanna.dev/renovate-graph/) or [the Renovate debug logs](https://github.com/renovatebot/renovate/issues/21455)
- Dependabot, via [dependabot-graph](https://gitlab.com/tanna.dev/dependabot-graph/)

## License

Licensed under the Apache-2.0 license. Documentation licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode">Creative Commons Attribution Non Commercial Share Alike 4.0 International</a>.
